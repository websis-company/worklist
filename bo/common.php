<?php
//define("DEVELOPMENT_TEST", false || $_SERVER['REMOTE_ADDR'] == '187.189.11.234');
define("DEVELOPMENT_TEST", false );
if( DEVELOPMENT_TEST ){//|| $_SERVER['REMOTE_ADDR'] == '189.203.203.29'
	ini_set("display_errors",1);
	error_reporting(E_ALL);
}else{
	error_reporting(0);
}

//error_reporting(E_ALL);
/**
 * This function loads ALL classes required by the application
 *
 * @param string $class_name
 * @return void
 */
function __autoload( $class_name ){

	$debug = DEVELOPMENT_TEST;
	$path = array ( );
	
	$pathToLib = 'libs/';
	
	if ( strpos( $class_name, 'Zend_') === 0 ){
		$path[] = $pathToLib . str_replace( "_", "/", $class_name ) . ".php";
	}
	else {
		$path[] = $pathToLib . str_replace( "_", "/", $class_name ) . ".class.php";
		$path[] = $pathToLib . "VicomStudio/" . str_replace( "_", "/", $class_name ) . ".class.php";
		$path[] = $pathToLib . "VicomStudio/Exceptions/" . str_replace( "_", "/", $class_name ) . ".class.php";
		$path[] = $pathToLib . "PEAR/" . str_replace( "_", "/", $class_name ) . ".php";
		$path[] = $pathToLib . "PhpMailer/class." . $class_name . ".php";
		$path[] = $pathToLib . "PayPal/" . str_replace( "_", "/", $class_name ) . ".php";
		$path[] = $pathToLib . "VPCPaymentConnection/" . str_replace( "_", "/", $class_name ) . ".class.php";
	}
	
	if ( $debug ){
		echo "Looking for <b>$class_name</b><br>";
	}
	
	$found = false;
	
	$include_paths = explode( PATH_SEPARATOR, get_include_path() );
	
	foreach ( $path as $filename ){
		if ( $debug ){
			echo "Filename: " . $filename . ' ... ';
		}
		if(DEVELOPMENT_TEST){
			if ( include_once ( $filename ) ){
				if ( $debug ){
					echo "<B>FOUND</B><br>";
				}
				return;
			} else{
				if ( $debug ){
					echo "Not found <br>";
				}
			}
		}else{
			if ( @include_once ( $filename ) ){
				if ( $debug ){
					echo "<B>FOUND</B><br>";
				}
				return;
			} else{
				if ( $debug ){
					echo "Not found <br>";
				}
			}
		}
	}
	if ( $debug ){
		echo "<br>";
	}
	if ( $found ){
		return true;
	}
	
	// The following line is required so no Fatal error is returned and the exception is launched
	eval( 'class ' . $class_name . ' extends ClassNotFoundException {}' );
	
	// Prepare the exception descriptive text.
	$txt = "The class <b>$class_name</b> could not be found under the following paths:<br>";
	$txt .= '<ul><li>' . join( '</li><li>', $path ) . '</li></ul>';
	
	throw new ClassNotFoundException( $txt );
}

/**
 * Debug function that writes to the output buffer the var_dump() information
 * from the given $var using $msg as label. The var_dump information is 
 * <pre>formatted.
 *
 * @param mixed $var The variable to debug
 * @param string $msg The label to prepend before the debug information
 */
function debug( $var, $msg = null ){
	echo '<pre>';
	echo $msg;
	ob_start();
	var_dump( $var );
	$debug = ob_get_contents();
	ob_end_clean();
	
	$debug = htmlentities( $debug );
	
	echo str_replace( "=>\n", "=>", $debug );
	
	echo '</pre>';
}

@session_start();

// IDENTIFICACI�N DE CHAR ENCODINGS SEG�N LA REGI�N
//setlocale(LC_CTYPE, 'en_US');  // Estados Unidos
//setlocale(LC_CTYPE, 'es_MX');  // M�xico
//setlocale(LC_CTYPE, 'fr_FR');  // Francia

define( 'ABS_PATH', dirname( __FILE__ ) );
define( 'SYSTEM_DIRECTORY', 'bo/');
define( 'BO_DIRECTORY', 'bo/');
define( 'DIRECTORY_BO_RELATIVE', 'bo/');
// Initialize default constants
Config::Initialize();

// Initialize the error handler
ExceptionHandler::initialize();

//define ("CHARSET_PROJECT", empty($CHARSET) ? "ISO-8859-1" : $CHARSET);
define ("CHARSET_PROJECT", empty($CHARSET) ? "UTF-8" : $CHARSET);
define ("CHARSET_PROJECT_DB", "utf8");

header('Content-Type: text/html; charset=' . CHARSET_PROJECT);

if( defined('LANGUAGE') ){
	$lang = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
	if(LANGUAGE != $lang){
			eCommerce_SDO_Core_Application_LanguageManager::SetActualLanguage( LANGUAGE );
	}
}


/*$pathToLib = 'libs/';
include_once $pathToLib . "PHPExcel.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/APC.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/CacheBase.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/DiscISAM.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/ICache.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/Igbinary.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/Memcache.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/Memory.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/MemoryGZip.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/MemorySerialized.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/PHPTemp.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/SQLite.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/SQLite3.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorage/Wincache.php";
include_once $pathToLib . "PHPExcel/CachedObjectStorageFactory.php";
include_once $pathToLib . "PHPExcel/CalcEngine/CyclicReferenceStack.php";
include_once $pathToLib . "PHPExcel/CalcEngine/Logger.php";
include_once $pathToLib . "PHPExcel/Calculation/Database.php";
include_once $pathToLib . "PHPExcel/Calculation/DateTime.php";
include_once $pathToLib . "PHPExcel/Calculation/Engineering.php";
include_once $pathToLib . "PHPExcel/Calculation/Exception.php";
include_once $pathToLib . "PHPExcel/Calculation/ExceptionHandler.php";
include_once $pathToLib . "PHPExcel/Calculation/Financial.php";
include_once $pathToLib . "PHPExcel/Calculation/FormulaParser.php";
include_once $pathToLib . "PHPExcel/Calculation/FormulaToken.php";
include_once $pathToLib . "PHPExcel/Calculation/Function.php";

include_once $pathToLib . "PHPExcel/Calculation/Functions.php";
include_once $pathToLib . "PHPExcel/Calculation/Logical.php";
include_once $pathToLib . "PHPExcel/Calculation/LookupRef.php";
include_once $pathToLib . "PHPExcel/Calculation/MathTrig.php";
include_once $pathToLib . "PHPExcel/Calculation/Statistical.php";
include_once $pathToLib . "PHPExcel/Calculation/TextData.php";
include_once $pathToLib . "PHPExcel/Calculation/Token/Stack.php";
include_once $pathToLib . "PHPExcel/Calculation.php";
include_once $pathToLib . "PHPExcel/Cell/AdvancedValueBinder.php";
include_once $pathToLib . "PHPExcel/Cell/DataType.php";
include_once $pathToLib . "PHPExcel/Cell/DataValidation.php";
include_once $pathToLib . "PHPExcel/Cell/DefaultValueBinder.php";
include_once $pathToLib . "PHPExcel/Cell/Hyperlink.php";
include_once $pathToLib . "PHPExcel/Cell/IValueBinder.php";
include_once $pathToLib . "PHPExcel/Cell.php";
include_once $pathToLib . "PHPExcel/Chart/DataSeries.php";
include_once $pathToLib . "PHPExcel/Chart/DataSeriesValues.php";
include_once $pathToLib . "PHPExcel/Chart/Exception.php";
include_once $pathToLib . "PHPExcel/Chart/Layout.php";
include_once $pathToLib . "PHPExcel/Chart/Legend.php";
include_once $pathToLib . "PHPExcel/Chart/PlotArea.php";
include_once $pathToLib . "PHPExcel/Chart/Renderer/jpgraph.php";

include_once $pathToLib . "PHPExcel/Chart/Title.php";
include_once $pathToLib . "PHPExcel/Chart.php";
include_once $pathToLib . "PHPExcel/Comment.php";
include_once $pathToLib . "PHPExcel/DocumentProperties.php";
include_once $pathToLib . "PHPExcel/DocumentSecurity.php";
include_once $pathToLib . "PHPExcel/Exception.php";
include_once $pathToLib . "PHPExcel/HashTable.php";
include_once $pathToLib . "PHPExcel/IComparable.php";
include_once $pathToLib . "PHPExcel/IOFactory.php";
include_once $pathToLib . "PHPExcel/locale/cs/config";
include_once $pathToLib . "PHPExcel/locale/cs/functions";
include_once $pathToLib . "PHPExcel/locale/da/config";
include_once $pathToLib . "PHPExcel/locale/da/functions";
include_once $pathToLib . "PHPExcel/locale/de/config";
include_once $pathToLib . "PHPExcel/locale/de/functions";
include_once $pathToLib . "PHPExcel/locale/en/uk/config";
include_once $pathToLib . "PHPExcel/locale/es/config";
include_once $pathToLib . "PHPExcel/locale/es/functions";
include_once $pathToLib . "PHPExcel/locale/fi/config";
include_once $pathToLib . "PHPExcel/locale/fi/functions";
include_once $pathToLib . "PHPExcel/locale/fr/config";
include_once $pathToLib . "PHPExcel/locale/fr/functions";
include_once $pathToLib . "PHPExcel/locale/hu/config";
include_once $pathToLib . "PHPExcel/locale/hu/functions";
include_once $pathToLib . "PHPExcel/locale/it/config";
include_once $pathToLib . "PHPExcel/locale/it/functions";
include_once $pathToLib . "PHPExcel/locale/nl/config";
include_once $pathToLib . "PHPExcel/locale/nl/functions";
include_once $pathToLib . "PHPExcel/locale/no/config";
include_once $pathToLib . "PHPExcel/locale/no/functions";
include_once $pathToLib . "PHPExcel/locale/pl/config";
include_once $pathToLib . "PHPExcel/locale/pl/functions";
include_once $pathToLib . "PHPExcel/locale/pt/br/config";
include_once $pathToLib . "PHPExcel/locale/pt/br/functions";
include_once $pathToLib . "PHPExcel/locale/pt/config";
include_once $pathToLib . "PHPExcel/locale/pt/functions";
include_once $pathToLib . "PHPExcel/locale/ru/config";
include_once $pathToLib . "PHPExcel/locale/ru/functions";
include_once $pathToLib . "PHPExcel/locale/sv/config";
include_once $pathToLib . "PHPExcel/locale/sv/functions";
include_once $pathToLib . "PHPExcel/locale/tr/config";
include_once $pathToLib . "PHPExcel/locale/tr/functions";
include_once $pathToLib . "PHPExcel/NamedRange.php";
include_once $pathToLib . "PHPExcel/Reader/Abstract.php";
include_once $pathToLib . "PHPExcel/Reader/CSV.php";
include_once $pathToLib . "PHPExcel/Reader/DefaultReadFilter.php";
include_once $pathToLib . "PHPExcel/Reader/Excel2003XML.php";
include_once $pathToLib . "PHPExcel/Reader/Excel2007/Chart.php";
include_once $pathToLib . "PHPExcel/Reader/Excel2007/Theme.php";
include_once $pathToLib . "PHPExcel/Reader/Excel2007.php";
include_once $pathToLib . "PHPExcel/Reader/Excel5/Escher.php";
include_once $pathToLib . "PHPExcel/Reader/Excel5.php";
include_once $pathToLib . "PHPExcel/Reader/Exception.php";
include_once $pathToLib . "PHPExcel/Reader/Gnumeric.php";
include_once $pathToLib . "PHPExcel/Reader/HTML.php";
include_once $pathToLib . "PHPExcel/Reader/IReader.php";
include_once $pathToLib . "PHPExcel/Reader/IReadFilter.php";
include_once $pathToLib . "PHPExcel/Reader/OOCalc.php";
include_once $pathToLib . "PHPExcel/Reader/SYLK.php";
include_once $pathToLib . "PHPExcel/ReferenceHelper.php";
include_once $pathToLib . "PHPExcel/RichText/ITextElement.php";
include_once $pathToLib . "PHPExcel/RichText/Run.php";
include_once $pathToLib . "PHPExcel/RichText/TextElement.php";
include_once $pathToLib . "PHPExcel/RichText.php";
include_once $pathToLib . "PHPExcel/Settings.php";
include_once $pathToLib . "PHPExcel/Shared/CodePage.php";
include_once $pathToLib . "PHPExcel/Shared/Date.php";
include_once $pathToLib . "PHPExcel/Shared/Drawing.php";
include_once $pathToLib . "PHPExcel/Shared/Escher/DgContainer/SpgrContainer/SpContainer.php";
include_once $pathToLib . "PHPExcel/Shared/Escher/DgContainer/SpgrContainer.php";
include_once $pathToLib . "PHPExcel/Shared/Escher/DgContainer.php";
include_once $pathToLib . "PHPExcel/Shared/Escher/DggContainer/BstoreContainer/BSE/Blip.php";
include_once $pathToLib . "PHPExcel/Shared/Escher/DggContainer/BstoreContainer/BSE.php";
include_once $pathToLib . "PHPExcel/Shared/Escher/DggContainer/BstoreContainer.php";
include_once $pathToLib . "PHPExcel/Shared/Escher/DggContainer.php";
include_once $pathToLib . "PHPExcel/Shared/Escher.php";
include_once $pathToLib . "PHPExcel/Shared/Excel5.php";
include_once $pathToLib . "PHPExcel/Shared/File.php";
include_once $pathToLib . "PHPExcel/Shared/Font.php";

include_once $pathToLib . "PHPExcel/Shared/JAMA/CholeskyDecomposition.php";
include_once $pathToLib . "PHPExcel/Shared/JAMA/EigenvalueDecomposition.php";
include_once $pathToLib . "PHPExcel/Shared/JAMA/LUDecomposition.php";
include_once $pathToLib . "PHPExcel/Shared/JAMA/Matrix.php";
include_once $pathToLib . "PHPExcel/Shared/JAMA/QRDecomposition.php";
include_once $pathToLib . "PHPExcel/Shared/JAMA/SingularValueDecomposition.php";
include_once $pathToLib . "PHPExcel/Shared/JAMA/utils/Error.php";
include_once $pathToLib . "PHPExcel/Shared/JAMA/utils/Maths.php";
include_once $pathToLib . "PHPExcel/Shared/OLE/ChainedBlockStream.php";
include_once $pathToLib . "PHPExcel/Shared/OLE/PPS/File.php";
include_once $pathToLib . "PHPExcel/Shared/OLE/PPS/Root.php";
include_once $pathToLib . "PHPExcel/Shared/OLE/PPS.php";
include_once $pathToLib . "PHPExcel/Shared/OLE.php";
include_once $pathToLib . "PHPExcel/Shared/OLERead.php";
include_once $pathToLib . "PHPExcel/Shared/PasswordHasher.php";

include_once $pathToLib . "PHPExcel/Shared/PCLZip/pclzip.lib.php";

include_once $pathToLib . "PHPExcel/Shared/String.php";
include_once $pathToLib . "PHPExcel/Shared/TimeZone.php";
include_once $pathToLib . "PHPExcel/Shared/trend/bestFitClass.php";
include_once $pathToLib . "PHPExcel/Shared/trend/exponentialBestFitClass.php";
include_once $pathToLib . "PHPExcel/Shared/trend/linearBestFitClass.php";
include_once $pathToLib . "PHPExcel/Shared/trend/logarithmicBestFitClass.php";
include_once $pathToLib . "PHPExcel/Shared/trend/polynomialBestFitClass.php";
include_once $pathToLib . "PHPExcel/Shared/trend/powerBestFitClass.php";
include_once $pathToLib . "PHPExcel/Shared/trend/trendClass.php";
include_once $pathToLib . "PHPExcel/Shared/XMLWriter.php";
include_once $pathToLib . "PHPExcel/Shared/ZipArchive.php";
include_once $pathToLib . "PHPExcel/Shared/ZipStreamWrapper.php";
include_once $pathToLib . "PHPExcel/Style/Alignment.php";
include_once $pathToLib . "PHPExcel/Style/Border.php";
include_once $pathToLib . "PHPExcel/Style/Borders.php";
include_once $pathToLib . "PHPExcel/Style/Color.php";
include_once $pathToLib . "PHPExcel/Style/Conditional.php";
include_once $pathToLib . "PHPExcel/Style/Fill.php";
include_once $pathToLib . "PHPExcel/Style/Font.php";
include_once $pathToLib . "PHPExcel/Style/NumberFormat.php";
include_once $pathToLib . "PHPExcel/Style/Protection.php";
include_once $pathToLib . "PHPExcel/Style/Supervisor.php";
include_once $pathToLib . "PHPExcel/Style.php";
include_once $pathToLib . "PHPExcel/Worksheet/AutoFilter/Column/Rule.php";
include_once $pathToLib . "PHPExcel/Worksheet/AutoFilter/Column.php";
include_once $pathToLib . "PHPExcel/Worksheet/AutoFilter.php";
include_once $pathToLib . "PHPExcel/Worksheet/BaseDrawing.php";
include_once $pathToLib . "PHPExcel/Worksheet/CellIterator.php";
include_once $pathToLib . "PHPExcel/Worksheet/ColumnDimension.php";
include_once $pathToLib . "PHPExcel/Worksheet/Drawing/Shadow.php";
include_once $pathToLib . "PHPExcel/Worksheet/Drawing.php";
include_once $pathToLib . "PHPExcel/Worksheet/HeaderFooter.php";
include_once $pathToLib . "PHPExcel/Worksheet/HeaderFooterDrawing.php";
include_once $pathToLib . "PHPExcel/Worksheet/MemoryDrawing.php";
include_once $pathToLib . "PHPExcel/Worksheet/PageMargins.php";
include_once $pathToLib . "PHPExcel/Worksheet/PageSetup.php";
include_once $pathToLib . "PHPExcel/Worksheet/Protection.php";
include_once $pathToLib . "PHPExcel/Worksheet/Row.php";
include_once $pathToLib . "PHPExcel/Worksheet/RowDimension.php";
include_once $pathToLib . "PHPExcel/Worksheet/RowIterator.php";
include_once $pathToLib . "PHPExcel/Worksheet/SheetView.php";
include_once $pathToLib . "PHPExcel/Worksheet.php";
include_once $pathToLib . "PHPExcel/WorksheetIterator.php";
include_once $pathToLib . "PHPExcel/Writer/Abstract.php";
include_once $pathToLib . "PHPExcel/Writer/CSV.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/Chart.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/Comments.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/ContentTypes.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/DocProps.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/Drawing.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/Rels.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/StringTable.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/Style.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/Theme.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/Workbook.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/Worksheet.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007/WriterPart.php";
include_once $pathToLib . "PHPExcel/Writer/Excel2007.php";
include_once $pathToLib . "PHPExcel/Writer/Excel5/BIFFwriter.php";
include_once $pathToLib . "PHPExcel/Writer/Excel5/Escher.php";
include_once $pathToLib . "PHPExcel/Writer/Excel5/Font.php";
include_once $pathToLib . "PHPExcel/Writer/Excel5/Parser.php";
include_once $pathToLib . "PHPExcel/Writer/Excel5/Workbook.php";
include_once $pathToLib . "PHPExcel/Writer/Excel5/Worksheet.php";
include_once $pathToLib . "PHPExcel/Writer/Excel5/Xf.php";
include_once $pathToLib . "PHPExcel/Writer/Excel5.php";
include_once $pathToLib . "PHPExcel/Writer/Exception.php";
include_once $pathToLib . "PHPExcel/Writer/HTML.php";
include_once $pathToLib . "PHPExcel/Writer/IWriter.php";
include_once $pathToLib . "PHPExcel/Writer/PDF/Core.php";
include_once $pathToLib . "PHPExcel/Writer/PDF/DomPDF.php";
include_once $pathToLib . "PHPExcel/Writer/PDF/mPDF.php";
include_once $pathToLib . "PHPExcel/Writer/PDF/tcPDF.php";
include_once $pathToLib . "PHPExcel/Writer/PDF.php";*/


/*function listar_directorios_ruta($ruta){
	// abrir un directorio y listarlo recursivo	
	if (is_dir($ruta)){
		if ($dh = opendir($ruta)){
			//echo "<br /><strong>$ruta</strong>"; // Aqui se imprime el nombre de la carpeta o directorio
			while (($file = readdir($dh)) !== false){
			//if (is_dir($ruta . $file) && $file!="." && $file!="..") // Si se desea mostrar solo directorios
				if ($file!="." && $file!="..") // Si se desea mostrar directorios y archivos
				{
					//solo si el archivo es un directorio, distinto que "." y ".."
					if(!is_dir($ruta.$file)){
						echo"<br />$ruta$file"; // Aqui se imprime el nombre del Archivo con su ruta relativa
						//include_once($ruta.$file);
					}

					listar_directorios_ruta($ruta . $file.'/'); // Ahora volvemos a llamar la función
				}
			}
			closedir($dh);
		}
 	}
 }

$ruta = '../libs/PHPExcel/';
listar_directorios_ruta($ruta);*/

?>