<?php
/**
 * This class is the base class for all business entities handled by this system
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 * @abstract 
 * @package eCommerce
 */
abstract class eCommerce_Entity extends Object {
	
	public function __construct(){
		parent::__construct();
	}
	
}
?>