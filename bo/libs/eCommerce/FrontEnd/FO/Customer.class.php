<?php

class eCommerce_FrontEnd_FO_Customer extends eCommerce_FrontEnd_FO{
	const CMD_NOT_AUTHENTIFICATION = 'register,saveRegister,saveRegisterWithAddress';
	/**
	 * @var eCommerce_DAO_Event
	 */
	public function __construct(){
		parent::__construct();
	}

	public function execute(){
		$cmd = $this->getCommand();
		//verify if the comand needs authentification
		$cmdNotAuthentification = split(',', self::CMD_NOT_AUTHENTIFICATION ); 
		if( !in_array( $cmd, $cmdNotAuthentification) ){
			eCommerce_FrontEnd_FO_Authentification::verifyAuthentification();
		}		
		switch( $cmd ){
			case 'register':
				//$this->_userRegister();
				$this->_userRegisterWithAddress();
			break;
			
			case 'saveRegister':
				$this->_saveRegister();
			break;
				
			case 'saveRegisterWithAddress':
				$this->_saveRegisterWithAddress();
			break;

			case 'save':
				$this->_save();
			break;
			
			case 'edit':
				$this->_edit();
			break;
			
			case 'saveAddress':
				$this->_saveAddress();
				break;
				
			case 'editAddress':
				$this->_editAddress();
			
			case 'listOrders':
				$this->_listOrders();
			break;
			
			case 'viewOrder':
				$this->_viewOrder();
				break;
			
			default:
			case 'info':
				$this->_info();
			break;
		}
	}
	
	protected function _userRegister(){
		$add1 = $this->tpl->trans('there_are_errors');
		$add2 = $this->tpl->trans('create').' '.$this->tpl->trans('account');
		$add3 = $this->tpl->trans('register');

		$user = (!empty($this->tpl->user) ) ? $this->tpl->user : new eCommerce_Entity_User_Profile();
		$errors = (!empty($this->tpl->errors) ) ? $this->tpl->errors : new Validator_ErrorHandler();
		
		$this->tpl->assign('user', $user);
		$this->tpl->assign('errors', $errors);
		
		$this->tpl->assign( 'strCmd', 'saveRegister' );
		
		$this->tpl->assign( 'strSubtitles', $add2);
		$this->tpl->assign( 'strButton', $add3 );
		
		$this->tpl->display( "UserRegister.php" );
	}
	
	protected function _userRegisterWithAddress(){
		$add1 = $this->tpl->trans('there_are_errors');
		$add2 = $this->tpl->trans('create').' '.$this->tpl->trans('account');
		$add3 = $this->tpl->trans('register');

		$user = (!empty($this->tpl->user) ) ? $this->tpl->user : new eCommerce_Entity_User_Profile();
		$errors = (!empty($this->tpl->errors) ) ? $this->tpl->errors : new Validator_ErrorHandler();
		$userAddress = (!empty($this->tpl->userAddress) ) ? $this->tpl->userAddress : new eCommerce_Entity_Order_Address();
		
		$this->tpl->assign('user', $user);
		$this->tpl->assign('errors', $errors);
		$this->tpl->assign( 'address', $userAddress );
		
		$this->tpl->assign( 'strCmd', 'saveRegisterWithAddress' );
		
		$this->tpl->assign( 'strSubtitles', $add2);
		$this->tpl->assign( 'strButton', $add3 );
		
		$this->tpl->display( "UserRegister.php" );
	}

	
	protected function _save(){
		$userSession = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$user = eCommerce_SDO_Core_Application_Profile::LoadById( $userSession->getProfileId() );
		
		if( !empty( $_REQUEST ) && isset($_REQUEST['entity']) ){
			$entity = $_REQUEST['entity'];
			
			$user->setFirstName( $entity['first_name'] );
			$user->setLastName( $entity['last_name'] );
			$user->setPassword($entity['password']);
			$user->setConfirmPassword($entity['password']);
			$user->setEmail( $entity['email'] );
			$user->setRole( eCommerce_SDO_User::CUSTOMER_ROLE );
			
		}
		try{
			$add1 = $this->tpl->trans('there_are_errors');
			$add2 = ucfirst(strtolower($this->tpl->trans('the').' '.$this->tpl->trans('user').' "' . $user->getEmail() . '" '.$this->tpl->trans('has_been').' '.$this->tpl->trans('deleted')));
				
			$user = eCommerce_SDO_User::SaverUserProfile( $user );
			
			$userSession = eCommerce_FrontEnd_Util_UserSession::SetIdentity( $user );
			
			$this->tpl->assign( 'strSuccess', $add2 );
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $add1, $e->getErrors() ) );
		}
		$this->tpl->assign('user', $user);
		//$this->_edit();
		$this->_info();
	}
	
	protected function _edit(){
		$add1 = $this->tpl->trans('edit').' '.$this->tpl->trans('account');
		$add2 = $this->tpl->trans('save');

		$user = (!empty($this->tpl->user) ) ? $this->tpl->user : eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$errors = (!empty($this->tpl->errors) ) ? $this->tpl->errors : new Validator_ErrorHandler();
		
		$this->tpl->assign( 'strSubtitles', $add1 );
		$this->tpl->assign( 'strCmd'      , "save" );
		$this->tpl->assign( 'strButton'   , $add2 );
		
		$this->tpl->assign("user", $user);
		$this->tpl->assign('errors', $errors);
		
		$this->tpl->display("UserEditProfile.php");
	}

	protected function _saveRegister(){
		$add1 = $this->tpl->trans('there_are_errors');
		$user = new eCommerce_Entity_User_Profile();
		$address = new eCommerce_Entity_User_Address();
		
		$this->tpl->assign( 'address', $address );
		
		$db = new eCommerce_SDO_Core_DB();
		$db->beginTransaction();
		try{
			if( !empty( $_REQUEST ) && isset($_REQUEST['entity']) ){
				$entity = $_REQUEST['entity'];
				$user->setFirstName( $entity['first_name'] );
				$user->setLastName( $entity['last_name'] );
				$user->setPassword($entity['password']);
				$user->setConfirmPassword($entity['confirm_password']);
				$user->setEmail( $entity['email'] );
				$user->setRole( eCommerce_SDO_User::CUSTOMER_ROLE );
			}
			$add2 = ucfirst(strtolower($this->tpl->trans('the').' '.$this->tpl->trans('user').' "' . $user->getEmail() . '" '.$this->tpl->trans('has_been').' '.$this->tpl->trans('deleted')));
			$this->tpl->assign('user', $user);
			
			$profile = eCommerce_SDO_User::SaverUserProfile( $user );
			$this->tpl->assign('strSuccess', $add2 );

			$db->commit();
			
			$auth = new eCommerce_FrontEnd_FO_Authentification();
			$auth->login($user->getEmail(), $user->getPassword());
		}
		catch( Exception $e ){
			$db->rollback();
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $add1, $e->getErrors() ) );
			$this->_userRegister();
		}
	}
	
	protected function _saveRegisterWithAddress(){
		$add1 = $this->tpl->trans('there_are_errors');
		$user = new eCommerce_Entity_User_Profile();
		$address = new eCommerce_Entity_User_Address();
		
		$this->tpl->assign( 'address', $address );
		
		$db = new eCommerce_SDO_Core_DB();
		$db->beginTransaction();
		try{			
			if( !empty( $_REQUEST ) && isset($_REQUEST['entity']) ){
				$entity = $_REQUEST['entity'];
				$user->setFirstName( $entity['first_name'] );
				$user->setLastName( $entity['last_name'] );
				$user->setPassword($entity['password']);
				$user->setConfirmPassword($entity['confirm_password']);
				$user->setEmail( $entity['email'] );
				$user->setRole( eCommerce_SDO_User::CUSTOMER_ROLE );
			}
			$add2 = ucfirst(strtolower($this->tpl->trans('the').' '.$this->tpl->trans('user').' "' . $user->getEmail() . '" '.$this->tpl->trans('has_been').' '.$this->tpl->trans('deleted')));
			$this->tpl->assign('user', $user);
			
			//-------------operation for the address
			$entityAddress = $this->getParameter( 'entityAddress', false, array() );
			
			$address->setStreet( $entityAddress['street'] );
			$address->setStreet2( $entityAddress['street2'] );
			$address->setStreet3( $entityAddress['street3'] );
			$address->setStreet4( $entityAddress['street4'] );
			$address->setCity( $entityAddress['city'] );
			$address->setState( $entityAddress['state'] );
			$address->setCountry( $entityAddress['country'] );
			$address->setZipCode( $entityAddress['zip_code'] );
			$address->setPhone( $entityAddress['phone'] );
			$address->setPhone2( $entityAddress['phone2'] );
			$address->setPhone3( $entityAddress['phone3'] );
			$address->setPhone4( $entityAddress['phone4'] );
			$address->setContactMethod( $entityAddress['contact_method'] );
			
			$this->tpl->assign('userAddress', $address);
			
			$profile = eCommerce_SDO_User::SaverUserProfile( $user );
			$this->tpl->assign('strSuccess', $add2 );
	
			$address->setProfileId( $profile->getProfileId() );
			$this->tpl->assign('userAddress', $address);
		
			eCommerce_SDO_User::SaveUserAddress( $address );
						
			$db->commit();
			
			$auth = new eCommerce_FrontEnd_FO_Authentification();
			$auth->login($user->getEmail(), $user->getPassword());
		}
		catch( Exception $e ){
			$db->rollback();
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $add1, $e->getErrors() ) );
			$this->_userRegisterWithAddress();
		}
	}
	
	protected function _info(){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$address = eCommerce_SDO_User::LoadUserAddress( $user->getProfileId() );
		$this->tpl->assign( 'address', $address );
		$this->tpl->assign( "user", $user);
		
		$this->tpl->display( "UserInfo.php" );
	}
	
	protected function _listOrders(){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, 'creation_date DESC' ),
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 15 )
		);
		
		$result = eCommerce_SDO_Core_Application_Order::Search( $search, $user->getProfileId() );
		
		$exportToExcel = $this->getParameter( "toExcel" );
		if( $exportToExcel ){
			
			$orders = $result->getResults();
			$data = array();
			foreach( $orders as $order ){
				$data[] = get_object_vars( $product );
			}
			//$this->exportToExcel( $data );
		}
		else{
			$this->tpl->assign( 'options', $result );
			$this->tpl->display("UserOrderMain.php");
		}
		
	}
	
	protected function _viewOrder(){
		$add1 = $this->tpl->trans('view').' '.$this->tpl->trans('order');
		$id = $this->getParameter();
		$order = eCommerce_SDO_Order::LoadFullById( $id );
		
		$actualProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$actualProfile = $actualProfile->getProfileId();

		//ATENTION 
		//verified if the property of the order is the actual user
		if( $order->getProfileId() != $actualProfile){
			session_destroy();
			echo "La orden de compra no esta relacionada al e-mail porporcionado";
			die();
			//$this->_listOrders();
			//$this->redirect('user_info.php?cmd=viewOrder&id=660');
		}	
		else{
			session_destroy();
			$html = eCommerce_SDO_Order::GetHTMLByOrderId( $id );
			$this->tpl->assign( "strSubtitles" , $add1);
			$this->tpl->assign( "orderHtml" , $html);
			$this->tpl->assign( "order" , $order);
			$this->tpl->display("UserOrderView.php");
		}
	
	}
	
	
	protected function _editAddress(){
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$profileId = $user->getProfileId();
		
		$address = eCommerce_SDO_User::LoadUserAddress( $profileId );
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler( ) );
		$this->editAddress( $address );
	}
	
	public function editAddress( eCommerce_Entity_User_Address $address ){
		
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$profileId = $user->getProfileId();
		
		$this->tpl->assign( 'profile', $user ); 
		$this->tpl->assign( 'address', $address );
		$this->tpl->display( "UserEditAddress.php" );
	}
	
	public function _saveAddress(){
		$add1 = $this->tpl->trans('there_are_errors');
		$user = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		$profileId = $user->getProfileId();
		
		$entity = $this->getParameter( 'entity', false, array() );
		
		$address = new eCommerce_Entity_User_Address();
		$address->setProfileId( $profileId );
		$address->setStreet( $entity['street'] );
		$address->setStreet2( $entity['street2'] );
		$address->setStreet3( $entity['street3'] );
		$address->setStreet4( $entity['street4'] );
		$address->setCity( $entity['city'] );
		$address->setState( $entity['state'] );
		$address->setCountry( $entity['country'] );
		$address->setZipCode( $entity['zip_code'] );
		$address->setPhone( $entity['phone'] );
		$address->setPhone2( $entity['phone2'] );
		$address->setPhone3( $entity['phone3'] );
		$address->setPhone4( $entity['phone4'] );
		$address->setContactMethod( $entity['contact_method'] );
		try {
			eCommerce_SDO_User::SaveUserAddress( $address );
			$this->_info();
		}
		catch( eCommerce_SDO_Core_Validator_Exception $e ) {
			$this->tpl->assign( 'errors', new Validator_ErrorHandler( $add1, $e->getErrors() ) );
			$this->editAddress( $address );
		}
		
	}
}
?>