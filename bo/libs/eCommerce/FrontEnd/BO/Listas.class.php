<?php
class eCommerce_FrontEnd_BO_Listas extends eCommerce_FrontEnd_BO {
public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Listas::SearchListas( $search, $extraConditions );
}	
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	public function __construct(){
		date_default_timezone_set("America/Mexico_City");
		parent::__construct();
		$this->idFields = 'listas_id';
	}
	public function execute(){
		//$this->checkPermission();
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
				//$this->_add();
				//break;
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'list':
			default:
				$this->_list();
				break;
		}
	}
	protected function _list(){
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);
		$extraConditions = array();

		if(isset($_REQUEST["action"]) && $_REQUEST["action"] == "viewImportant"){
			$extraConditions[] = array( "SQL"=>"importante = 'Si'");
		}
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );
		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Listas';
			$viewConfig['title'] = 'Listas';
			$viewConfig['id'] = $this->idFields;
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			$viewConfig["hiddenColums"]= array('listas_id','status','startdate','enddate');
			$viewConfig['columNamesOverride'] = array('name'=>'Nombre','description'=>'Descripciòn','startdate'=>'Fecha Inicio','enddate'=>'Fecha Fin');
			$this->tpl->assign( 'viewConfig', $viewConfig );
			$this->tpl->assign( 'options', $result );
			$this->tpl->display(  "Listas/list.php" );
		}
	}
	protected function _add(){
		try{
			$Listas = new eCommerce_Entity_Listas();
			$Listas = eCommerce_SDO_Listas::SaverListas( $Listas );
			$this->_edit( $Listas->getListasId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		$entity = eCommerce_SDO_Listas::LoadListas( $id );
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		$this->edit( $entity, $this->getCommand() );
	}
	public function edit( $entity, $cmd ){
		$this->tpl->assign( 'object',     $entity );
		$viewConfig = array();
		$form = array();
		$form['name'] = 'ListasForm';
		$form['elements'] = array();
		/***********************************************************************************************************/
		$form['elements'][] = array( 'title' => 'ListasId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[listas_id]', 'id'=>'listas_id', 'value'=> $entity->getListasId() );
		$form['elements'][] = array( 'title' => 'Name:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[name]', 'id'=>'name', 'value'=> $entity->getName() );
		$form['elements'][] = array( 'title' => 'Description:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[description]', 'id'=>'description', 'value'=> $entity->getDescription() );
		$form['elements'][] = array( 'title' => 'Startdate:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[startdate]', 'id'=>'startdate', 'value'=> $entity->getStartdate() );
		$form['elements'][] = array( 'title' => 'Enddate:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[enddate]', 'id'=>'enddate', 'value'=> $entity->getEnddate() );
		$form['elements'][] = array( 'title' => 'Status:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[status]', 'id'=>'status', 'value'=> $entity->getStatus() );
		/***********************************************************************************************************/
		//$viewConfig['id'] = 'noticia_evento_id';
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Lista de Tareas';
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );
		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getListasId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
		$this->tpl->display( 'Listas/edit.php' );
	}
	protected function exportToExcel( $data, $fileName = '' ){
		$rowO = $data[0];
		$contentHtml .= '<table border="1">';
		$contentHtml .= '<tr>
		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>
		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>
		</tr>';
		$contentHtml .= '<tr>';
		foreach($rowO as $key => $row){
			$key = str_replace("_",' ',$key);
			$key = ucwords($key);
			$contentHtml .= '<th align="center">'.$key.'</th>';
		}
		$contentHtml .= '</tr>';
		$i = 0;
		foreach($data as $row){
			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';
			$contentHtml .= '<tr>';
			foreach($row as $rowElement){
				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';
			}
			$contentHtml .= '</tr>';
		}
		$contentHtml .= '</table>';
		$total_bytes = strlen($contentHtml);
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);
		echo $contentHtml;
	}
	protected function _save(){
		if ( !empty( $_REQUEST )){
			//$entity = $_REQUEST['entity'];
			$entity = $_REQUEST;

			$Listas = new eCommerce_Entity_Listas();
			// Save type
			try{
					$rangeDate   = $entity["rangeTime"];
					if(empty($rangeDate)){
						$startdate   = NULL;
						$enddate     = NULL;
					}else{
						$rangeDate_a = explode(" - ", $rangeDate);
						$startdate   = date('Y-m-d H:i:s',strtotime($rangeDate_a[0]));
						$enddate     = date('Y-m-d H:i:s',strtotime($rangeDate_a[1]));
					}//end if
					

					/******************************************************/
					/******************************************************/
					$txt = (empty($entity['listas_id']) ? '' : $entity['listas_id']);$Listas->setListasId( strip_tags($txt) );
					$txt = (empty($entity['name']) ? '' : $entity['name']);$Listas->setName( strip_tags($txt) );
					$txt = (empty($entity['description']) ? '' : $entity['description']);$Listas->setDescription( strip_tags($txt) );
					$Listas->setStartdate($startdate);
					$Listas->setEnddate($enddate);
					$txt = (empty($entity['status']) ? '' : $entity['status']);$Listas->setStatus( strip_tags($txt) );
					$txt = (empty($entity['important']) ? '' : $entity['important']);$Listas->setImportante( strip_tags($txt) );
		  			/******************************************************/
		  			/*****************************************************/
					/*
					$ListasTmp = eCommerce_SDO_Listas::LoadListas( $Listas->getListasId() );
					$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('ListasImages');
					$imageHandler->setArrImages( $ListasTmp->getArrayImages() );
					$imageHandler->setUploadFileDirectory('files/images/noticias/');
					$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Listas->getListasId() );
					$imageHandler->setValidTypes( array('word','image') );
					$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
					$Listas->setArrayImages( implode(',',$ListasTmp->getArrayImages()) );
					$imageHandler->setMaximum( 1 );
					$arrImages = $imageHandler->proccessImages();
					$Listas->setArrayImages( $arrImages );
					*/
				eCommerce_SDO_Listas::SaverListas( $Listas );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Listas "' . $Listas->getListasId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Listas,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}
	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
			try{
				$Listas = eCommerce_SDO_Listas::DeleteListas($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Listas "' . $Listas->getListasId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		$this->_list();
	}
}
?>