<?php
class eCommerce_FrontEnd_BO_Collection extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Collection::SearchCollection( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	
	public function __construct(){
		parent::__construct();
		$this->idFields = 'collection_id';
	}

	public function execute(){
// 		$accessManager = new eCommerce_Access_Collection( $this );
// 		$accessManager->checkPermission(eCommerce_Access::COLLECTION_ALL_PERMISSIONS);
// 		$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
// 				$accessManager->checkPermission(eCommerce_Access::COLLECTION_DELETE);
				$this->_delete();
				break;
			case 'add':
// 				$accessManager->checkPermission(eCommerce_Access::COLLECTION_ADD);
// 				$this->_add();
// 				break;
			case 'edit':
// 				$accessManager->checkPermission(eCommerce_Access::COLLECTION_EDIT);
				$this->_edit();
				break;
			case 'save':
// 				$accessManager->checkPermission(eCommerce_Access::COLLECTION_SAVE);
				$this->_save();
				break;
			case 'list':
			default:
// 				$accessManager->checkPermission(eCommerce_Access::COLLECTION_LIST);
				$this->_list();
				break;
		}
	}

	protected function _list(){
	
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = $this->tpl->trans('collection');
			$viewConfig['title'] = $this->tpl->trans('collection');
			$viewConfig['id'] = $this->idFields;
			
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			
			$viewConfig["hiddenColums"]= array('collection_id', 'array_image');
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->display(  "Collection/list.php" );
		}
	}
	
	protected function _add(){
		try{
			$Collection = new eCommerce_Entity_Collection();
			$Collection = eCommerce_SDO_Collection::SaverCollection( $Collection );
			$this->_edit( $Collection->getCollectionId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}
	
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		
		$entity = eCommerce_SDO_Collection::LoadCollection( $id );
		
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		
		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'CollectionForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/
		
$form['elements'][] = array( 'title' => 'CollectionId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[collection_id]', 'id'=>'collection_id', 'value'=> $entity->getCollectionId() );
$form['elements'][] = array( 'title' => $this->trans('name').":", 'type'=>'text', 'options'=>array(), 'name'=>'entity[name]', 'id'=>'name', 'value'=> $entity->getName() );
$form['elements'][] = array( 'title' => $this->trans('description').":", 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[description]', 'id'=>'description', 'value'=> $entity->getDescription() );
$checada = (($entity->getActual()=='1')?'checked':'');
$form['elements'][] = array( 'title' => 'Actual:', 'type'=>'checkbox', 'options'=>array(), 'name'=>'entity[actual]', 'id'=>'actual', 'value' => '1', 'checada'=>$checada );
$form['elements'][] = array( 'title' => $this->trans('image')." Principal:", 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[image]', 'id'=>'image', 'value'=> $entity->getImage(), 'max_files'=>1 );
$form['elements'][] = array( 'title' => $this->trans('image')."es:", 'type'=>'array_files', 'options'=>array(), 'name'=>'entity[array_image]', 'id'=>'array_image', 'value'=> $entity->getArrayImage(), 'max_files'=>500 );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' ' . $this->tpl->trans('collection');
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getCollectionId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
		
			$this->tpl->display( 'Collection/edit.php' );
	}

	protected function exportToExcel( $data, $fileName = '' ){

		

		$rowO = $data[0];

		

		$contentHtml .= '<table border="1">';

		$contentHtml .= '<tr>

		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>

		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>

		</tr>';

		

		$contentHtml .= '<tr>';

		

		foreach($rowO as $key => $row){

			$key = str_replace("_",' ',$key);

			$key = ucwords($key);

			$contentHtml .= '<th align="center">'.$key.'</th>';

		}

		$contentHtml .= '</tr>';

		

		$i = 0;

		foreach($data as $row){

			

			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';

			$contentHtml .= '<tr>';

			foreach($row as $rowElement){

				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';

			}

			$contentHtml .= '</tr>';

		}

		

		$contentHtml .= '</table>';

		

		

		$total_bytes = strlen($contentHtml);

		header("Content-type: application/vnd.ms-excel");

		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);

		echo $contentHtml;

	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$Collection = new eCommerce_Entity_Collection();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['collection_id']) ? '' : $entity['collection_id']);$Collection->setCollectionId( strip_tags($txt) );
$txt = (empty($entity['name']) ? '' : $entity['name']);$Collection->setName( strip_tags($txt) );
$txt = (empty($entity['description']) ? '' : $entity['description']);$Collection->setDescription( strip_tags($txt) );
$txt = (empty($entity['actual']) ? '' : $entity['actual']);$Collection->setActual( strip_tags($txt) );
  			/******************************************************/
  			/*****************************************************/
  			
	
				
				$CollectionTmp = eCommerce_SDO_Collection::LoadCollection( $Collection->getCollectionId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('image');
				$imageHandler->setArrImages( $CollectionTmp->getImage() );
				
				$imageHandler->setUploadFileDirectory('files/images/collections/');
				$imageHandler->setUploadVersionDirectory('web/');
				$imageHandler->setUploadFileDescription($Collection->getCollectionId() );
				$imageHandler->setValidTypes( array('image') );
				$imageHandler->setAccessType('public' );
				
				$Collection->setImage( implode(',',$CollectionTmp->getImage()) );
				$arrVersions = array();
				$arrVersions[] = array('version'=>'small',	'path'=>'images/collection/',	'width'=>309, 'height'=>410);
				$imageHandler->setArrVersions($arrVersions);
				
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Collection->setImage( $arrImages );
				

				$CollectionTmp = eCommerce_SDO_Collection::LoadCollection( $Collection->getCollectionId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('array_image');
				$imageHandler->setArrImages( $CollectionTmp->getArrayImage() );
				
				$imageHandler->setUploadFileDirectory('files/images/collections/');
				$imageHandler->setUploadVersionDirectory('web/');
				$imageHandler->setUploadFileDescription($Collection->getCollectionId() );
				$imageHandler->setValidTypes( array('image') );
				$imageHandler->setAccessType('public' );
				$arrVersions = array();
				$arrVersions[] = array('version'=>'small',	'path'=>'images/collection/',	'width'=>309, 'height'=>410);
				$arrVersions[] = array('version'=>'medium',	'path'=>'images/collection/',	'width'=>500, 'height'=>504);
				$imageHandler->setArrVersions($arrVersions);
				$Collection->setArrayImage( implode(',',$CollectionTmp->getArrayImage()) );
				$imageHandler->setMaximum( 500 );
				$arrImages = $imageHandler->proccessImages();
				$Collection->setArrayImage( $arrImages );
				
				
	
				eCommerce_SDO_Collection::SaverCollection( $Collection );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Collection "' . $Collection->getCollectionId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Collection,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
		
			try{
				$Collection = eCommerce_SDO_Collection::DeleteCollection($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Collection "' . $Collection->getCollectionId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
				
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}
	
	
	

}
?>