<?php
class eCommerce_FrontEnd_BO_CategoryInDifferentLanguage extends eCommerce_FrontEnd_BO {


public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_CategoryInDifferentLanguage::SearchCategoryInDifferentLanguage( $search, $extraConditions );
}	


/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;

	public function __construct(){
		parent::__construct();
		
	}

	public function execute(){
		eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
		
		$this->checkPermission();
		
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'list':
			default:
				$this->_list();
				break;
		}
	}

	protected function _list(){

$CategoryId = empty($this->tpl->CategoryId) ? $this->getParameter('CategoryId') : $this->tpl->CategoryId;

		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, 10 )
		);
		$extraConditions = array();
		
		if( !empty($CategoryId) ){
	$extraConditions[] = array( "columName"=>"category_id","value"=>$CategoryId,"isInteger"=>true);
		}
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		
		
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
		
			$users = $result->getResults();
			$data = array();
			foreach($users as $user){
				$data[] = get_object_vars( $user );
			}
			$this->exportToExcel( $data );

		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = $this->tpl->trans('category').' '.$this->tpl->trans('in').' '.$this->tpl->trans('other_language');
			$viewConfig['title'] = $this->tpl->trans('category').' '.$this->tpl->trans('in').' '.$this->tpl->trans('other_languages');
			$viewConfig['id'] = 'category_in_different_language_id';
			
			$viewConfig["hiddenColums"]= array('array_images','category_id','description','category_in_different_language_id');
			$viewConfig['hiddenFields'] = array("CategoryId"=>$CategoryId);
			$viewConfig['columNamesOverride'] = array('language_id'=>$this->tpl->trans('language'), 'name'=>$this->tpl->trans('name'), 'description'=>$this->tpl->trans('description'));
			
			$this->tpl->assign( 'viewConfig', $viewConfig );
			
			$this->tpl->assign( 'options', $result );
			
			$this->tpl->assign('CategoryId', $CategoryId);
			
			$this->tpl->display(  "categoryInDiferrentLanguage/list.php" );
		}
	}
		
	protected function _edit(){
		$id = $this->getParameter();

		$entity = eCommerce_SDO_CategoryInDifferentLanguage::LoadCategoryInDifferentLanguage( $id );
		
		$pId = $entity->getCategoryId();
		if( empty($pId) ){
			$CategoryId = $this->getParameter('CategoryId');
			$this->tpl->assign('CategoryId', $CategoryId);
			$entity->setCategoryId($CategoryId);
		}else{
			$this->tpl->assign('CategoryId', $entity->getCategoryId() );
		}
		
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );

		$this->edit( $entity, $this->getCommand() );

	}

	public function edit( $entity, $cmd ){

		$this->tpl->assign( 'object',     $entity );
		
		$viewConfig = array();
		$form = array();

		
		$form['name'] = 'CategoryInDifferentLanguageForm';
		$form['elements'] = array();
		
		
		/***********************************************************************************************************/

$languages = array('EN'=>$this->tpl->trans('english'), 'SP'=>$this->tpl->trans('spanish'));
unset($languages[eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage()]);
$form['elements'][] = array( 'title' => 'CategoryInDifferentLanguageId', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[category_in_different_language_id]', 'id'=>'category_in_different_language_id', 'value'=> $entity->getCategoryInDifferentLanguageId() );
$form['elements'][] = array( 'title' => 'CategoryId', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[category_id]', 'id'=>'category_id', 'value'=> $entity->getCategoryId() );
$form['elements'][] = array( 'title' => $this->tpl->trans('language').': *', 'type'=>'select', 'options'=>$languages, 'name'=>'entity[language_id]', 'id'=>'language_id', 'value'=> $entity->getLanguageId() );
$form['elements'][] = array( 'title' => $this->tpl->trans('name').': *', 'type'=>'text', 'options'=>array(), 'name'=>'entity[name]', 'id'=>'name', 'value'=> $entity->getName() );
$form['elements'][] = array( 'title' => $this->tpl->trans('description').':', 'type'=>'textarea', 'options'=>array(), 'name'=>'entity[description]', 'id'=>'description', 'value'=> $entity->getDescription() );
		/***********************************************************************************************************/
		
		//$viewConfig['id'] = 'noticia_evento_id';
		
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' '.$this->tpl->trans('the(f)').' '.$this->tpl->trans('category').' '.$this->tpl->trans('in').' '.$this->tpl->trans('other_language');
		
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );

		$this->tpl->display( 'categoryInDiferrentLanguage/edit.php' );
	}

	protected function exportToExcel( $data ){
		$xls = new ExcelWriter( 'Noticias_' . date('Y-m-d'), 'CategoryInDifferentLanguage' );
		$xls->writeHeadersAndDataFromArray( $data );
	}

	protected function _save(){
		
		if ( !empty( $_REQUEST ) && isset( $_REQUEST['entity'] ) ){
	  		
			$entity = $_REQUEST['entity'];
  			
			$CategoryInDifferentLanguage = new eCommerce_Entity_CategoryInDifferentLanguage();
			

			// Save type
			try{
			
			
			/******************************************************/
			/******************************************************/
			
$txt = (empty($entity['category_in_different_language_id']) ? '' : $entity['category_in_different_language_id']);$CategoryInDifferentLanguage->setCategoryInDifferentLanguageId( strip_tags($txt) );
$txt = (empty($entity['category_id']) ? '' : $entity['category_id']);$CategoryInDifferentLanguage->setCategoryId( strip_tags($txt) );
$txt = (empty($entity['language_id']) ? '' : $entity['language_id']);$CategoryInDifferentLanguage->setLanguageId( strip_tags($txt) );
$txt = (empty($entity['name']) ? '' : $entity['name']);$CategoryInDifferentLanguage->setName( strip_tags($txt) );
$txt = (empty($entity['description']) ? '' : $entity['description']);$CategoryInDifferentLanguage->setDescription( strip_tags($txt) );
  			/******************************************************/
  			/*****************************************************/
  			
				
				/*
				$CategoryInDifferentLanguageTmp = eCommerce_SDO_CategoryInDifferentLanguage::LoadCategoryInDifferentLanguage( $CategoryInDifferentLanguage->getCategoryInDifferentLanguageId() );
				
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('CategoryInDifferentLanguageImages');
				$imageHandler->setArrImages( $CategoryInDifferentLanguageTmp->getArrayImages() );
				
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $CategoryInDifferentLanguage->getCategoryInDifferentLanguageId() );
				
				$imageHandler->setValidTypes( array('word','image') );
				
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('categoryInDiferrentLanguage') );
				
				$CategoryInDifferentLanguage->setArrayImages( implode(',',$CategoryInDifferentLanguageTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$CategoryInDifferentLanguage->setArrayImages( $arrImages );
				*/
				
				
				eCommerce_SDO_CategoryInDifferentLanguage::SaverCategoryInDifferentLanguage( $CategoryInDifferentLanguage );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the(f)').' '.$this->tpl->trans('category').' '.$this->tpl->trans('in').' '.$this->tpl->trans('other_language').' "' . $CategoryInDifferentLanguage->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved(f)')));
								
				$this->tpl->assign( 'CategoryId', $CategoryInDifferentLanguage->getCategoryId() );
				
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $CategoryInDifferentLanguage,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}

	protected function _delete(){
		$id = $this->getParameter();
			try{
				$CategoryInDifferentLanguage = eCommerce_SDO_CategoryInDifferentLanguage::DeleteCategoryInDifferentLanguage($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the(f)').' '.$this->tpl->trans('category').' '.$this->tpl->trans('in').' '.$this->tpl->trans('other_language').' "' . $CategoryInDifferentLanguage->getName() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted(f)')));
				$this->tpl->assign('CategoryId', $CategoryInDifferentLanguage->getCategoryId() );
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		
		$this->_list();
	}

}
?>