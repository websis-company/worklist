<?php
class eCommerce_FrontEnd_BO_Tareas extends eCommerce_FrontEnd_BO {
public function getSDOSearch( $search, $extraConditions ){
	return eCommerce_SDO_Tareas::SearchTareas( $search, $extraConditions );
}	
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/
	/**
	 * @var eCommerce_DAO_Event
	 */
	protected $search;
	protected $idFields;
	public function __construct(){
		date_default_timezone_set("America/Mexico_City");
		parent::__construct();
		$this->idFields = 'tareas_id';
	}
	public function execute(){
		$this->checkPermission();
		$cmd = $this->getCommand();
		switch( $cmd ){
			case 'delete':
				$this->_delete();
				break;
			case 'add':
			case 'edit':
				$this->_edit();
				break;
			case 'save':
				$this->_save();
				break;
			case 'view':
			case 'list':
			default:
				$this->_list();
				break;
		}
	} 

	protected function _list(){
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);
		$extraConditions = array();

		if(isset($_REQUEST["filter"]) && $_REQUEST["filter"] == "today"){
			$extraConditions[] = array("SQL"=>"startdate LIKE '%".date('Y-m-d')."%'");
		}elseif(isset($_REQUEST["filter"]) && $_REQUEST["filter"] == "pending"){
			$extraConditions[] = array("SQL"=>"enddate < '".date('Y-m-d')."' && status != 'Terminado'");
		}elseif(isset($_REQUEST["filter"]) && $_REQUEST["filter"] == "finish"){
			$extraConditions[] = array("SQL"=>"status =  'Terminado'");
		}

		if(isset($_REQUEST["action"]) && $_REQUEST["action"] == "view"){
			$extraConditions[] = array("SQL"=>"listas_id = ".$_REQUEST["listas_id"]);
		}

		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		$exportToExcel = $this->getParameter( 'toExcel' );
		if ( $exportToExcel ){
			$regiters = $result->getResults();
			$data = array();
			foreach($regiters as $regiter){
				$data[] = get_object_vars( $regiter );
			}
			$this->exportToExcel( $data );
		}
		else {
			$viewConfig = array();
			$viewConfig['name'] = 'Tareas';
			$viewConfig['title'] = 'Tareas';
			$viewConfig['id'] = $this->idFields;
			//---------------------------
			if(strpos($viewConfig['id'],",") > -1){
				$ids = explode(",",$viewConfig['id']);
				$id = $ids[0];
				unset($ids[0]);
				$viewConfig['id'] = '{$'.$id.'}';
				foreach($ids as $id){
					$viewConfig['id'] .= ',{$'.$id.'}';
				}
			}else{
				$viewConfig['id'] = '{$'.$viewConfig['id'].'}';
			}
			//---------------------------
			$viewConfig["hiddenColums"] = array('tareas_id','status');
			$viewConfig['columNamesOverride'] = array('listas_id'=>'Lista','name'=>'Tarea','description'=>'Descripción','startdate'=>'Inicio','enddate'=>'Fin');

			$this->tpl->assign( 'viewConfig', $viewConfig );
			$this->tpl->assign( 'options', $result );
			$this->tpl->display(  "Tareas/list.php" );
		}
	}

	protected function _getTodayWork(){
		$this->search = new eCommerce_Entity_Search(
			$this->getParameter( 'q', false, NULL ),
			$this->getParameter( 'o', false, '' ), //fecha DESC
			$this->getParameter( 'p', true, 1 ),
			$this->getParameter( 'k', true, -1 )
		);
		$extraConditions = array();
		$extraConditions[] = array("SQL"=>"startdate LIKE '%".date('Y-m-d')."%'");

		//$extraConditions[] = array( "columName"=>"type","value"=>'',"isInteger"=>false);
		$result = $this->getSDOSearch( $this->search, $extraConditions ); 
		return $result;
	}


	protected function _add(){
		try{
			$Tareas = new eCommerce_Entity_Tareas();
			$Tareas = eCommerce_SDO_Tareas::SaverTareas( $Tareas );
			$this->_edit( $Tareas->getTareasId() );
		}catch( eCommerce_SDO_Core_Validator_Exception $e){
			$this->tpl->assign( 'strError', $this->tpl->trans('try_again') );
			$this->_list();
		}
	}
	protected function _edit($id = null){
		//$id = $this->getParameter();
		$id = empty($id ) ? $this->getParameter('id',false,0) : $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
			}
		}
		//---------------------------
		$entity = eCommerce_SDO_Tareas::LoadTareas( $id );
		$this->tpl->assign( 'errors', new Validator_ErrorHandler() );
		$this->edit( $entity, $this->getCommand() );
	}
	public function edit( $entity, $cmd ){
		$this->tpl->assign( 'object',     $entity );
		$viewConfig = array();
		$form = array();
		$form['name'] = 'TareasForm';
		$form['elements'] = array();
		/***********************************************************************************************************/
		$form['elements'][] = array( 'title' => 'TareasId:', 'type'=>'hidden', 'options'=>array(), 'name'=>'entity[tareas_id]', 'id'=>'tareas_id', 'value'=> $entity->getTareasId() );
		$form['elements'][] = array( 'title' => 'ListasId:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[listas_id]', 'id'=>'listas_id', 'value'=> $entity->getListasId() );
		$form['elements'][] = array( 'title' => 'Name:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[name]', 'id'=>'name', 'value'=> $entity->getName() );
		$form['elements'][] = array( 'title' => 'Description:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[description]', 'id'=>'description', 'value'=> $entity->getDescription() );
		$form['elements'][] = array( 'title' => 'Startdate:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[startdate]', 'id'=>'startdate', 'value'=> $entity->getStartdate() );
		$form['elements'][] = array( 'title' => 'Enddate:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[enddate]', 'id'=>'enddate', 'value'=> $entity->getEnddate() );
		$form['elements'][] = array( 'title' => 'Status:', 'type'=>'text', 'options'=>array(), 'name'=>'entity[status]', 'id'=>'status', 'value'=> $entity->getStatus() );
		/***********************************************************************************************************/
		//$viewConfig['id'] = 'noticia_evento_id';
		$viewConfig['form'] = $form;
		$viewConfig['title'] = ( $cmd =='add' ? $this->tpl->trans('add') : $this->tpl->trans('edit') ) . ' Tareas';
		$this->tpl->assign( 'form', $form );	
		$this->tpl->assign( 'viewConfig', $viewConfig );
		//$this->tpl->assign( 'ArrayImages', $entity->getArrayImages() );
		$this->tpl->assign( 'cancelParams', $cmd == 'add' ? '?cmd=delete&id=' . $entity->getTareasId() . '' : '' );
		$this->tpl->assign( 'cancelConfirm', $cmd == 'add' ? 'if(confirm("'.$this->tpl->trans('?_confirm_delete').' '.strtolower($this->tpl->trans('recently_created_entry')).'?"))'  : '' );
			$this->tpl->display( 'Tareas/edit.php' );
	}
	protected function exportToExcel( $data, $fileName = '' ){
		$rowO = $data[0];
		$contentHtml .= '<table border="1">';
		$contentHtml .= '<tr>
		<td height="72" width="255" bgcolor="#cccccc"><img src="'.ABS_HTTP_URL.'/ima/logo.gif"></td>
		<td colspan="'.(count($rowO) - 1).'"><h2>Order Report - '.date("M d Y").'</h2></td>
		</tr>';
		$contentHtml .= '<tr>';
		foreach($rowO as $key => $row){
			$key = str_replace("_",' ',$key);
			$key = ucwords($key);
			$contentHtml .= '<th align="center">'.$key.'</th>';
		}
		$contentHtml .= '</tr>';
		$i = 0;
		foreach($data as $row){
			$bgcolor = (++$i % 2 == 0) ? 'bgcolor="#cccccc"' : '';
			$contentHtml .= '<tr>';
			foreach($row as $rowElement){
				$contentHtml .= '<td '.$bgcolor.' align="right">'.$rowElement.'</td>';
			}
			$contentHtml .= '</tr>';
		}
		$contentHtml .= '</table>';
		$total_bytes = strlen($contentHtml);
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: attachment; filename=".$fileName.".xls; size=" . $total_bytes);
		echo $contentHtml;
	}
	protected function _save(){
		if ( !empty( $_REQUEST )){
			$entity = $_REQUEST;
			$Tareas = new eCommerce_Entity_Tareas();
			// Save type
			try{
				$rangeDate   = $entity["rangeTime"];
				if(empty($rangeDate)){
					$startdate   = NULL;
					$enddate     = NULL;
				}else{
					$rangeDate_a = explode(" - ", $rangeDate);
					$startdate   = date('Y-m-d H:i:s',strtotime($rangeDate_a[0]));
					$enddate     = date('Y-m-d H:i:s',strtotime($rangeDate_a[1]));
				}//end if


				/******************************************************/
				/******************************************************/
				$txt = (empty($entity['tareas_id']) ? '' : $entity['tareas_id']);$Tareas->setTareasId( strip_tags($txt) );
				$txt = (empty($entity['listas_id']) ? '' : $entity['listas_id']);$Tareas->setListasId( strip_tags($txt) );
				$txt = (empty($entity['name']) ? '' : $entity['name']);$Tareas->setName( strip_tags($txt) );
				$txt = (empty($entity['description']) ? '' : $entity['description']);$Tareas->setDescription( strip_tags($txt) );
				$Tareas->setStartdate($startdate);
				$Tareas->setEnddate($enddate);
				$txt = (empty($entity['status']) ? '' : $entity['status']);$Tareas->setStatus( strip_tags($txt) );
	  			/******************************************************/
	  			/*****************************************************/
				/*
				$TareasTmp = eCommerce_SDO_Tareas::LoadTareas( $Tareas->getTareasId() );
				$imageHandler = eCommerce_SDO_ImageHandler::GetImageHanlderObject('TareasImages');
				$imageHandler->setArrImages( $TareasTmp->getArrayImages() );
				$imageHandler->setUploadFileDirectory('files/images/noticias/');
				$imageHandler->setUploadFileDescription('Imagen de Evento ' . $Tareas->getTareasId() );
				$imageHandler->setValidTypes( array('word','image') );
				$imageHandler->setAccessType( eCommerce_SDO_ImageHandler::GetValidAccessType('public') );
				$Tareas->setArrayImages( implode(',',$TareasTmp->getArrayImages()) );
				$imageHandler->setMaximum( 1 );
				$arrImages = $imageHandler->proccessImages();
				$Tareas->setArrayImages( $arrImages );
				*/
				eCommerce_SDO_Tareas::SaverTareas( $Tareas );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Tareas "' . $Tareas->getTareasId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('saved')));
				$this->_list();
			}
			catch( eCommerce_SDO_Core_Validator_Exception $e){
				$this->tpl->assign( 'errors', new Validator_ErrorHandler( $this->tpl->trans("there_are_errors"), $e->getErrors() ) );
				$oldCmd = $this->getParameter( 'old_cmd', false, null );
				$this->edit( $Tareas,  $oldCmd );
			}
		}
		else{
			throw new Exception($this->tpl->trans('not_enough_information').'. '.$this->tpl->trans('try_again'));
		}
	}
	protected function _delete(){
		//$id = $this->getParameter();
		$id = $this->getParameter('id',false,0);
		$id2Str = $id;
		//---------------------------
		if( strpos($id,",") > -1 ){
			$idF = explode(",",$this->idFields);
			$ArrId = explode(",",$id);
			$id = array();
			$id2Str = "(";
			for($i=0; $i< count($idF); $i++){
				$id[ $idF[$i] ] = (empty($ArrId[$i])) ? 0 : $ArrId[$i];
				$id2Str .=($i>0) ? ',' : '';
				$id2Str .= $ArrId[$i] . "";
			}
			$id2Str .= ")";
		}
		//---------------------------
			try{
				$Tareas = eCommerce_SDO_Tareas::DeleteTareas($id );
				$this->tpl->assign( 'strSuccess', $this->tpl->trans('the').' Tareas "' . $Tareas->getTareasId() . '" '.$this->tpl->trans('has_been').' '.strtolower($this->tpl->trans('deleted')));
			}
			catch(eCommerce_SDO_Core_Application_Exception $e) {
				//debug($e);
				$this->tpl->assign( 'strError', $e->getMessage() );
			}
		$this->_list();
	}
}
?>