<?php
/**
 * This class represents a Currecy for sale eStore
 *
 * @author Alberto Perez <alberto@vicomstudio.com>
 * @package eCommerce::Entity
 */
class eCommerce_Entity_Currency extends eCommerce_Entity {

	/**
	 * Currency ID
	 *
	 * @var int
	 */
	public $currency_id;
	
	/**
	 * Currency name
	 *
	 * @var string
	 */
	public $name;
	/**
	 * Currency description
	 * @var string
	 */
	public $description;
	
	function __construct(){
		parent::__construct();
	}

	/**
	 * @return string
	 */
	public function getCurrencyId(){
		return $this->currency_id;
	}

	/**
	 * @param int $currencyId
	 */
	public function setCurrencyId( $currencyId ){
		$this->currency_id = $currencyId;
	}

	/**
	 * @return string
	 */
	public function getDescription(){
		return $this->description;
	}
	
	/**
	 * @param string
	 */
	public function setDescription( $description ){
		return $this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName( $name ){
		$this->name = $name;
	}

}
?>