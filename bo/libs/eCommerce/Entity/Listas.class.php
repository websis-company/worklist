<?php
class eCommerce_Entity_Listas extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $listas_id;
public $name;
public $description;
public $startdate;
public $enddate;
public $importante;
public $status;


public function getListasId(){ 
return $this->listas_id;
}

public function setListasId( $ListasId){ 
return $this->listas_id = $ListasId;
}

public function getName(){ 
return $this->name;
}

public function setName( $Name){ 
return $this->name = $Name;
}

public function getDescription(){ 
return $this->description;
}

public function setDescription( $Description){ 
return $this->description = $Description;
}

public function getStartdate(){ 
return $this->startdate;
}

public function setStartdate( $Startdate){ 
return $this->startdate = $Startdate;
}

public function getEnddate(){ 
return $this->enddate;
}

public function setEnddate( $Enddate){ 
return $this->enddate = $Enddate;
}

public function getStatus(){ 
return $this->status;
}

public function setStatus( $Status){ 
return $this->status = $Status;
}

public function getImportante(){ 
return $this->importante;
}

public function setImportante( $Importante){ 
return $this->importante = $Importante;
}

}
?>