<?php
class eCommerce_Entity_Tareas extends eCommerce_Entity {

	public function __construct(){
		parent::__construct();
	}

public $tareas_id;
public $listas_id;
public $name;
public $description;
public $startdate;
public $enddate;
public $status;


public function getTareasId(){ 
return $this->tareas_id;
}

public function setTareasId( $TareasId){ 
return $this->tareas_id = $TareasId;
}

public function getListasId(){ 
return $this->listas_id;
}

public function setListasId( $ListasId){ 
return $this->listas_id = $ListasId;
}

public function getName(){ 
return $this->name;
}

public function setName( $Name){ 
return $this->name = $Name;
}

public function getDescription(){ 
return $this->description;
}

public function setDescription( $Description){ 
return $this->description = $Description;
}

public function getStartdate(){ 
return $this->startdate;
}

public function setStartdate( $Startdate){ 
return $this->startdate = $Startdate;
}

public function getEnddate(){ 
return $this->enddate;
}

public function setEnddate( $Enddate){ 
return $this->enddate = $Enddate;
}

public function getStatus(){ 
return $this->status;
}

public function setStatus( $Status){ 
return $this->status = $Status;
}

}
?>