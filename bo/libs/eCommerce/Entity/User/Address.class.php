<?php
class eCommerce_Entity_User_Address extends eCommerce_Entity_Util_Address {
	
	/**
	 * User Profile ID reference
	 *
	 * @var int
	 */
	public $profile_id;
	
	/**
	 * @return int
	 */
	public function getProfileId(){
		return $this->profile_id;
	}

	/**
	 * @param int $profileId
	 */
	public function setProfileId( $profileId ){
		$this->profile_id = $profileId;
	}
	
}
?>