<?php
class eCommerce_Entity_Util_ProfileIterator extends ArrayIterator {
	
	/**
	 * Returns the current Profile entity in the array
	 * @return eCommerce_Entity_User_Profile
	 */
	public function current(){
		return parent::current();
	}
	
}
?>