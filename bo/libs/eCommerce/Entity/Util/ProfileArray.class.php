<?php
class eCommerce_Entity_Util_ProfileArray extends ArrayObject {
	
	public function __construct( array $array = array() ){
		parent::__construct( $array, 0, "eCommerce_Entity_Util_ProfileIterator" );
	}
	
	/**
	 * @return eCommerce_Entity_Util_ProfileIterator
	 */
	public function getIterator(){
		return parent::getIterator();
	}
	
}
?>