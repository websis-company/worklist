<?php
class eCommerce_SDO_Core_Application_Security_Exception extends eCommerce_SDO_Core_Application_Exception {
	
	const ERR_CASE_NOT_CONTEMPLATED = 0;
	const ERR_USER_NOT_FOUND = 1;
	const ERR_INVALID_PASSWORD = 2;
	const ERR_SQL_EXCEPTION = 3;
	
}
?>