<?php
/**
 * This class provides access to manipulation of Categories in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_Profile extends eCommerce_SDO_Core_Application {
	
	const ADMIN_ROLE 		= 'Admin';
	const CUSTOMER_ROLE 	= 'Customer';
	const ANONYMOUS_ROLE	= 'Anonymous';
	
	/**
	 * Load the Profile from the given ID. Note: an empty object is returned if the ID doesn't exist
	 *
	 * @param int $profileId
	 * @return eCommerce_Entity_User_Profile
	 */
	public static function LoadById( $profileId ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadById( $profileId, true );       
		$profile = new eCommerce_Entity_User_Profile();   // Create new clean object to prevent old values being conserved
		$profile = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $profile );  // Convert array to to Object
		return $profile;
	}
	
	/**
	 * Load the Profile from the given email. Note: an empty object is returned if the email isn't found
	 *
	 * @param string $email
	 * @return eCommerce_Entity_User_Profile
	 */
	public static function LoadByEmail( $email ){
		
		$dao = self::GetDAO();
		$entity = $dao->loadByEmail( $email );
		if ( empty( $entity ) ){
			$entity = $dao->loadById( 0, true );       
		}
		$profile = new eCommerce_Entity_User_Profile();   // Create new clean object to prevent old values being conserved
		$profile = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $profile );  // Convert array to to Object
		return $profile;
	}
	
	/**
	 * Saves a new or existing Profile into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_User_Profile $category - The object to be saved
	 * @return eCommerce_Entity_User_Profile           - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function Save( eCommerce_Entity_User_Profile $profile, $validar_password = true){
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_Profile( $profile, $validar_password);
		
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}

		$confirm_password = $profile->getConfirmPassword();
		unset($profile->confirm_password);
		
		// 2. Save or Update Object
		$entity = new ArrayObject( $profile );        // Convert Object to Array
		//die('esta entrando');

		$dao = self::GetDAO();
		
		$dao->saveOrUpdate( $entity );
		// 3. Retrieve record from DB
		$profile = self::LoadById( $entity[ 'profile_id' ] );
		$profile->setConfirmPassword($confirm_password);  // necesario por si hay errores en Address, que no se borre el campo

		if ( $profile == new eCommerce_Entity_User_Profile() ){
			throw new Exception( "The Category saved could not be retrieved." );      
		}
		// 4. Return recently saved object
		return $profile;
		
	}
	
	/**
	 * Deletes a Category from the System
	 *
	 * @param int $categoryId                    - The ID of the category to be deleted
	 * @return eCommerce_Entity_Catalog_Category - The recently deleted category
	 * @throws eCommerce_SDO_Core_Application_Exception - When the category is not found or couldn't be deleted
	 */
	public static function Delete( $profileId ){
		// 1. Ensure the category actually exists
		$profile = self::LoadById( $profileId );
		
		if ( $profile == new eCommerce_Entity_User_Profile() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The profile to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Category
				$dao = self::GetDAO();
				$dao->delete( $profileId );
				
				// 3. Return the recently deleted Category
				return $profile;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Profile', 0, $e );
			}
		}
	}
	
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_Category
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_Profile();
	}
	
	/**
	 * Retrieves all the Categories available.
	 * 
	 * @return eCommerce_Entity_Util_CategoryArray
	 */
	public static function GetAll(){
		$dao = self::GetDAO();
		$arrProfiles = $dao->loadAll();                           // Load all categories as a 2D array
		$objProfiles = self::ParseArrayToObjectArray( $arrProfiles );
		return $objProfiles;
	}
	
	/**
	 * Performs a paged search for Categories
	 *
	 * @param eCommerce_Entity_Search $search - The search parameters
	 * @param string $role                    - The role to filter by. Null for all user roles	
	 * @return eCommerce_Entity_Search_Result_Category
	 */
	public static function Search( eCommerce_Entity_Search $search, $role = null ){
		try {
			$dao = self::GetDAO();
			if ( !empty( $role ) ){
				$dao->addExtraCondition( "role = '" . $dao->getDB()->escapeString( $role ) . "'" );
			}
			// Retrieve categories
			$arrProfiles = $dao->loadAllByParameters( $search->getKeywords(), $search->getSearchAsPhrase(),
			                                          $search->getOrderBy(), $search->getPage(),
			                                          $search->getResultsPerPage() );
			// Retrieve total pages
			$totalPages = $dao->loadAllByParameters( $search->getKeywords(), $search->getSearchAsPhrase(),
			                                         $search->getOrderBy(), $search->getPage(),
			                                         $search->getResultsPerPage(),
			                                         true );
			
			$result = new eCommerce_Entity_Search_Result_Profile( $search );
			$result->setTotalPages( $totalPages );
			$result->setResults( self::ParseArrayToObjectArray( $arrProfiles ) ); 
			return $result;
		}
		catch( SQLException $e ){
			throw new eCommerce_SDO_Core_Application_Exception( 
				'Error processing the search request. Please see the nested exception for details',
				0, $e );
		}
	}
	
	/**
	 * @param array $arrProfiles
	 * @return eCommerce_Entity_Util_ProfileArray
	 */
	protected static function ParseArrayToObjectArray( array $arrProfiles ){
		// Create the array that will hold the Category objects
		$objProfiles = new eCommerce_Entity_Util_ProfileArray(); 
		foreach( $arrProfiles as $arrProfile ){
			// Transform each array into object
			$objProfile = eCommerce_SDO_Core_Util_EntityManager::
			               ParseArrayToObject( $arrProfile, new eCommerce_Entity_User_Profile() );
			// Add object to category array  
			$objProfiles[ $objProfile->getProfileId() ] = $objProfile;  
		}
		return $objProfiles;
	}
	
	/**
	 * Retrieves the list of Roles available to users
	 *
	 * @return array<string>
	 * @todo Retrieve the list from the actual DB (enum)
	 */
	public static function GetRoles(){
		$ArrRoles = array();
		
		$db = new eCommerce_SDO_Core_DB();
		
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoUserProfile = self::GetDAO();
		
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoUserProfile->getTable() . " LIKE 'role'" );
		
		//$enumvalues[ "type" ] contents text similar like 
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );
		
		$enumvalues = explode(',', $enumvalues);
		
		foreach( $enumvalues as $enumvalue ){
			//$enumvalue contents text similar 
			// 'valueN' we need only valueN
			$ArrRoles[] = substr( $enumvalue, 1, -1 );
			
		}
		return $ArrRoles;
	}
	
	public static function GetEnumValues( $colum ){
		$ArrRoles = array();
	
		$db = new eCommerce_SDO_Core_DB();
	
		//we create the instance here because eCommerce/SDO/Core
		//is restringed to access directly for the FrontEnd
		$daoUserProfile = self::GetDAO();
	
		$enumvalues = $db->sqlGetRecord( "SHOW COLUMNS FROM " . $daoUserProfile->getTable() . " LIKE '".$colum."'" );
	
		//$enumvalues[ "type" ] contents text similar like
		// enum( 'value1', 'value2' ... 'valueN' ) we need only the values
		// 'value1', 'value2' ... 'valueN'
		$enumvalues = substr( $enumvalues[ "type" ], strlen('enum(')  , -1 );
	
		$enumvalues = explode(',', $enumvalues);
	
		foreach( $enumvalues as $enumvalue ){
			//$enumvalue contents text similar
			// 'valueN' we need only valueN
			$ArrRoles[ substr( $enumvalue, 1, -1 ) ] = substr( $enumvalue, 1, -1 );
	
		}
		return $ArrRoles;
	}
	
	public static function GetAllInArray($includeNull = false, $filtroForo = false ){
		$arr = array();
		$search = new eCommerce_Entity_Search('','last_name ASC',1,-1);
		$extraCond = array();
		if($filtroForo)$extraCond[] = array("SQL" => "(role = 'Vendedor' OR  role = 'Admin')");
		$registers = self::Search( $search, null, $extraCond)->getResults();
	
		if($includeNull){
			$arr[0] = "----Ninguno----";
		}
		foreach($registers as $register){
			$arr[ $register->getProfileId() ] = "{$register->getLastName()} {$register->getFirstName()}";
		}
		return $arr;
	}
	
	public static function PasswordReset($user_id){
		$psw = Util_String::generarClave();
		$user = self::LoadById($user_id);
		$user->setPassword(md5($psw));
		self::Save($user, false);
		return $psw;
	}
}
?>