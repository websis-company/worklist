<?php
/**
 * This class provides access to manipulation of Currency in the System.
 * 
 * @author Alberto Pérez <alberto@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_CountryManager extends eCommerce_SDO_Core_Application {
	
	public static function GetStateById( $stateId, $countryId ){
		$states = self::GetStatesOfCountry( $countryId );
		
		return $states[ $stateId ];
	}
	
	public static function GetCountriesByZone( $zone){
		$countries= self::GetCountries();
		$countriesReturn = array();
		foreach( $countries as $country ){
			if( $country["zone"] == $zone){
				$countriesReturn[] = $country;
			}
		}
		$countriesReturn = self::GetArrCountriesShortNames( $countriesReturn );
		
		return $countriesReturn;
	}
	
	public static function GetStatesOfCountryByZone( $countryShortName, $zone){
		$states = self::GetStatesOfCountry( $countryShortName );
		$statesReturn = array();
		foreach( $states as $state ){
			if( $state["zone"] == $zone){
				$statesReturn[] = $state;
			}
		}
		$statesReturn = self::GetStatesShortNames( $statesReturn );
		
		return $statesReturn;
	}
	
	public static function GetStatesOfCountry( $countryShortName, $htmlSelect = false, $selectName='', $selected = '', $id = '', $CSSclass='', $selectParams ='', $inAjax = false, $defaultText = '' ){
		$validShortNames = self::GetCountriesShortNames();
		$states = array();
		
		if( in_array( $countryShortName, $validShortNames ) ){
			
			switch( $countryShortName ){
				case 'MX':
					$states = (!$htmlSelect) ? Util_State_MXState::GetStates() :
					Util_State_MXState::GetHTMLSelect($selectName, $selected, $id, $CSSclass, $selectParams, $inAjax, $defaultText); 
					
				break;
				
				case 'US':
					$states = (!$htmlSelect) ? Util_State_USState::GetStates() :
					Util_State_USState::GetHTMLSelect($selectName, $selected, $id, $CSSclass, $selectParams, $inAjax, $defaultText);
				break;
				
				default:
					if( !$htmlSelect){
						$states = Util_State::GetStates();
					}else{
						$states = ($inAjax) ? Util_State::GetHTMLDivAndAjax( $selectName, $selected, $id, $CSSclass, $selectParams) :
						Util_State::GetHTMLSelect( $selectName, $selected, $id, $CSSclass, $selectParams);
					}

					
				break;
				
			}
		}else{
			if( !$htmlSelect){
				$states = Util_State::GetStates();
			}else{
				$states = ($inAjax) ? Util_State::GetHTMLDivAndAjax( $selectName, $selected, $id, $CSSclass, $selectParams) :
				Util_State::GetHTMLSelect( $selectName, $selected, $id, $CSSclass, $selectParams);
			}
		}
		return $states;
	}
	
	public static function GetCountries(){
		$countries = array();
		$countriesISOCodes = Util_CountryISOCodes::getCountries();
		
		foreach($countriesISOCodes as $key => $contry ){
			$countries[ $key ] = array( 'country_short_name' =>$key , 'name'=>$contry, 'zone'=> 0 );
		}
		
		//this agrupation could'll be used in the shipping fees
		
		//zone: North America 
		$countries[ "MX" ]["zone"] = 1;
		$countries[ "US" ]["zone"] = 1;
		$countries['CA']["zone"] =  1;// = 'Canada';
		
		$countries['AF']["zone"] = 1;
    $countries['AL']["zone"] =  1;// = 'Albania';
    $countries['DZ']["zone"] =  1;// = 'Algeria';
    $countries['AD']["zone"] =  1;// = 'Andorra';
    $countries['AO']["zone"] =  1;// = 'Angola';
    $countries['AI']["zone"] =  1;// = 'Anguilla';
    $countries['AG']["zone"] =  1;// = 'Antigua and Barbuda';
    $countries['AR']["zone"] =  1;// = 'Argentina';
    $countries['AM']["zone"] =  1;// = 'Armenia';
    $countries['AW']["zone"] =  1;// = 'Aruba';
    $countries['AU']["zone"] =  1;// = 'Australia';
    $countries['AT']["zone"] =  1;// = 'Austria';
    $countries['BS']["zone"] =  1;// = 'Bahamas';
    $countries['BH']["zone"] =  1;// = 'Bahrain';
    $countries['BB']["zone"] =  1;// = 'Barbados';
    $countries['BD']["zone"] =  1;// = 'BBangladesh';
    $countries['BE']["zone"] =  1;// = 'Belgium';
    $countries['BZ']["zone"] =  1;// = 'Belize';
    $countries['BM']["zone"] =  1;// = 'Bermuda';
    $countries['BT']["zone"] =  1;// = 'Bhutan';
    $countries['BO']["zone"] =  1;// = 'Bolivia';
    $countries['BW']["zone"] =  1;// = 'Botswana';
    $countries['BR']["zone"] =  1;// = 'Brazil';
    $countries['BN']["zone"] =  1;// = 'Brunei';
    $countries['BG']["zone"] =  1;// = 'Bulgaria';
    $countries['BI']["zone"] =  1;// = 'Burundi';
    $countries['KH']["zone"] =  1;// = 'Cambodia';
    $countries['CV']["zone"] =  1;// = 'Cape Verde';
    $countries['KY']["zone"] =  1;// = 'Cayman Islands';
    $countries['CF']["zone"] =  1;// = 'Central African Republic';
    $countries['CL']["zone"] =  1;// = 'Chile';
    $countries['CN']["zone"] =  1;// = 'China';
    $countries['CO']["zone"] =  1;// = 'Colombia';
    $countries['KM']["zone"] =  1;// = 'Comoros';
    $countries['CR']["zone"] =  1;// = 'Costa Rica';
    $countries['HR']["zone"] =  1;// = 'Croatia';
    $countries['CU']["zone"] =  1;// = 'Cuba';
    $countries['CY']["zone"] =  1;// = 'Cyprus';
    $countries['CZ']["zone"] =  1;// = 'Czech Republic';
    $countries['DK']["zone"] =  1;// = 'Denmark';
    $countries['DJ']["zone"] =  1;// = 'Djibouti';
    $countries['DO']["zone"] =  1;// = 'Dominican Republic';
    $countries['EC']["zone"] =  1;// = 'Ecuador';
    $countries['EG']["zone"] =  1;// = 'Egypt';
    $countries['SV']["zone"] =  1;// = 'El Salvador';
    $countries['EE']["zone"] =  1;// = 'Estonia';
    $countries['ET']["zone"] =  1;// = 'Ethiopia';
    $countries['FK']["zone"] = 1;// = 'Falkland Islands';
    $countries['FJ']["zone"] = 1;// = 'Fiji';
    $countries['FI']["zone"] = 1;// = 'Finland';
    $countries['FR']["zone"] = 1;// = 'France';
    $countries['GM']["zone"] = 1;// = 'Gambia';
    $countries['DE']["zone"] = 1;// = 'Germany';
    $countries['GH']["zone"] = 1;// = 'Ghana';
    $countries['GI']["zone"] = 1;// = 'Gibraltar';
    $countries['GR']["zone"] = 1;// = 'Greece';
    $countries['GT']["zone"] = 1;// = 'Guatemala';
    $countries['GN']["zone"] = 1;// = 'Guinea';
    $countries['GY']["zone"] = 1;// = 'Guyana';
    $countries['HT']["zone"] = 1;// = 'Haiti';
    $countries['HN']["zone"] = 1;// = 'Honduras';
    $countries['HK']["zone"] = 1;// = 'Hong Kong';
    $countries['HU']["zone"] = 1;// = 'Hungary';
    $countries['IS']["zone"] = 1;// = 'Iceland';
    $countries['IN']["zone"] = 1;// = 'India';
    $countries['ID']["zone"] = 1;// = 'Indonesia';
    $countries['IR']["zone"] = 1;// = 'Iran';
    $countries['IQ']["zone"] = 1;// = 'Iraq';
    $countries['IE']["zone"] = 1;// = 'Ireland';
    $countries['IL']["zone"] = 1;// = 'Israel';
    $countries['IT']["zone"] = 1;// = 'Italy';
    $countries['JM']["zone"] = 1;// = 'Jamaica';
    $countries['JP']["zone"] = 1;// = 'Japan';
    $countries['JO']["zone"] = 1;// = 'Jordan';
    $countries['KZ']["zone"] = 1;// = 'Kazakhstan';
    $countries['KE']["zone"] = 1;// = 'Kenya';
    $countries['KW']["zone"] = 1;// = 'Kuwait';
    $countries['LA']["zone"] = 1;// = 'Laos';
    $countries['LV']["zone"] = 1;// = 'Latvia';
    $countries['LB']["zone"] = 1;// = 'Lebanon';
    $countries['LS']["zone"] = 1;// = 'Lesotho';
    $countries['LR']["zone"] = 1;// = 'Liberia';
    $countries['LY']["zone"] = 1;// = 'Libya';
    $countries['LT']["zone"] = 1;// = 'Lithuania';
    $countries['LU']["zone"] = 1;// = 'Luxembourg';
    $countries['MO']["zone"] = 1;// = 'Macao';
    $countries['MG']["zone"] = 1;// = 'Madagascar';
    $countries['MW']["zone"] = 1;// = 'Malawi';
    $countries['MY']["zone"] = 1;// = 'Malaysia';
    $countries['MV']["zone"] = 1;// = 'Maldives';
    $countries['MT']["zone"] = 1;// = 'Malta';
    $countries['MR']["zone"] = 1;// = 'Mauritania';
    $countries['MU']["zone"] = 1;// = 'Mauritius';
    $countries['MX']["zone"] = 1;// = 'Mexico';
    $countries['MN']["zone"] = 1;// = 'Mongolia';
    $countries['MA']["zone"] = 1;// = 'Morocco';
    $countries['MZ']["zone"] = 1;// = 'Mozambique';
    $countries['MM']["zone"] = 1;// = 'Myanmar';
    $countries['NA']["zone"] = 1;// = 'Namibia';
    $countries['NP']["zone"] = 1;// = 'Nepal';
    $countries['AN']["zone"] = 1;// = 'Netherlands Antilles';
    $countries['NL']["zone"] = 1;// = 'Netherlands';
    $countries['NZ']["zone"] = 1;// = 'New Zealand';
    $countries['NI']["zone"] = 1;// = 'Nicaragua';
    $countries['NG']["zone"] = 1;// = 'Nigeria';
    $countries['KP']["zone"] = 1;// = 'North Korea';
    $countries['NO']["zone"] = 1;// = 'Norway';
    $countries['OM']["zone"] = 1;// = 'Oman';
    $countries['PK']["zone"] = 1;// = 'Pakistan';
    $countries['PA']["zone"] = 1;// = 'Panama';
    $countries['PG']["zone"] = 1;// = 'Papua New Guinea';
    $countries['PY']["zone"] = 1;// = 'Paraguay';
    $countries['PE']["zone"] = 1;// = 'Peru';
    $countries['PH']["zone"] = 1;// = 'Philippines';
    $countries['PL']["zone"] = 1;// = 'Poland';
    $countries['PT']["zone"] = 1;// = 'Portugal';
    $countries['QA']["zone"] = 1;// = 'Qatar';
    $countries['RO']["zone"] = 1;// = 'Romania';
    $countries['RU']["zone"] = 1;// = 'Russia';
    $countries['SH']["zone"] = 1;// = 'Saint Helena';
    $countries['WS']["zone"] = 1;// = 'Samoa';
    $countries['ST']["zone"] = 1;// = 'Sao Tome/Principe';
    $countries['SA']["zone"] = 1;// = 'Saudi Arabia';
    $countries['SC']["zone"] = 1;// = 'Seychelles';
    $countries['SL']["zone"] = 1;// = 'Sierra Leone';
    $countries['SG']["zone"] = 1;// = 'Singapore';
    $countries['SK']["zone"] = 1;// = 'Slovakia';
    $countries['SI']["zone"] = 1;// = 'Slovenia';
    $countries['SB']["zone"] = 1;// = 'Solomon Islands';
    $countries['SO']["zone"] = 1;// = 'Somalia';
    $countries['ZA']["zone"] = 1;// = 'South Africa';
    $countries['KR']["zone"] = 1;// = 'South Korea';
    $countries['ES']["zone"] = 1;// = 'Spain';
    $countries['LK']["zone"] = 1;// = 'Sri Lanka';
    $countries['SD']["zone"] = 1;// = 'Sudan';
    $countries['SR']["zone"] = 1;// = 'Suriname';
    $countries['SZ']["zone"] = 1;// = 'Swaziland';
    $countries['SE']["zone"] = 1;// = 'Sweden';
    $countries['CH']["zone"] = 1;// = 'Switzerland';
    $countries['SY']["zone"] = 1;// = 'Syria';
    $countries['TW']["zone"] = 1;// = 'Taiwan';
    $countries['TZ']["zone"] = 1;// = 'Tanzania';
    $countries['TH']["zone"] = 1;// = 'Thailand';
    $countries['TO']["zone"] = 1;// = 'Tonga';
    $countries['TT']["zone"] = 1;// = 'Trinidad/Tobago';
    $countries['TN']["zone"] = 1;// = 'Tunisia';
    $countries['TR']["zone"] = 1;// = 'Turkey';
    $countries['UG']["zone"] = 1;// = 'Uganda';
    $countries['UA']["zone"] = 1;// = 'Ukraine';
    $countries['AE']["zone"] = 1;// = 'United Arab Emirates';
    $countries['GB']["zone"] = 1;// = 'United Kingdom';
    $countries['US']["zone"] = 1;// = 'United States';
    $countries['UY']["zone"] = 1;// = 'Uruguay';
    $countries['VU']["zone"] = 1;// = 'Vanuatu';
    $countries['VE']["zone"] = 1;// = 'Venezuela';
    $countries['VN']["zone"] = 1;// = 'Viet Nam';
    $countries['ZM']["zone"] = 1;// = 'Zambia';
    $countries['ZW']["zone"] = 1;// = 'Zimbabwe';
    
		return $countries;
	}
	
	protected static function GetArrCountriesShortNames( $ArrCountries ){
		$shortNames = array();
		foreach( $ArrCountries as $country ){
			$shortNames[] = $country[ 'country_short_name' ];
		}
		return $shortNames;
	}
	
	protected static function GetCountriesShortNames(){
		$Countries = self::GetCountries();
		$shortNames = array();
		foreach( $Countries as $country ){
			$shortNames[] = $country[ 'country_short_name' ];
		}
		return $shortNames;
	}
	
	protected static function GetStatesShortNames( $states ){
		$shortNames = array();
		foreach( $states as $state ){
			$shortNames[] = $state[ 'state_short_name' ];
		}
		return $shortNames;
	}

	public static function GetCountryById( $countryId ){
		$Countries = self::GetCountries();
		return $Countries[ $countryId ]; 
	}

	public function GetHTMLSelect( $selectName, $selected = '', $id = '', $CSSclass='', $selectParams ='', $defaultText = '' ){
		$countries = self::GetCountries();
		if( !empty($countries) ){
			$id = ( $id = '' ) ? $selectName : $id;
			$html = '<select name="' . $selectName . '" id="' . $id . '" class="' . $CSSclass . '" ' . $selectParams . '>';
			if(!empty($defaultText)) {
				$html .= '<option class="' . $CSSclass . '" value="">' . $defaultText . '</option>';
			}
			foreach( $countries as $code => $country ){
				
				$name = $country["name"];
				
				$selectedCodeHTML = ( $selected == $code || $selected == $name ) ? 'selected="SELECTED"' : '';
				$html .= '<option class="' . $CSSclass . '" value="' . $code . '" ' . $selectedCodeHTML .' >' . $name . '</option>';
			}
			$html .= '</select>';
		}else{
			$html = '<input type="text" class="' . $CSSclass . '" name="' . $selectName . '" id="' . $id . '" value="' . $selected .'" >';	
		}
		return $html;
	}	
		
}
?>