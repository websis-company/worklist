<?php
/**
 * This class provides access to manipulation of Profile Addresses in the System.
 * 
 * @author Alejandro Rivera <alejandro@vicomstudio.com>
 */
class eCommerce_SDO_Core_Application_UserAddress extends eCommerce_SDO_Core_Application {
	
	/**
	 * Loads the Profile Address from the given ID. 
	 * Note: An empty Address object is returned if no Address existed before. 
	 * The Profile ID attribute will be the same as function parameter
	 *
	 * @param int $profileId
	 * @return eCommerce_Entity_User_Address
	 */
	public static function LoadById( $profileId ){
		$dao = self::GetDAO();
		$entity = $dao->loadById( $profileId, true );       
		$address = new eCommerce_Entity_User_Address();   // Create new clean object to prevent old values being conserved
		$address = eCommerce_SDO_Core_Util_EntityManager::
		            ParseArrayToObject( $entity, $address );  // Convert array to to Object
		
		$addProfileId = $address->getProfileId();
		if ( empty( $addProfileId) ){
			$address->setProfileId( $profileId );
		}
		
		return $address;
	}
	
	/**
	 * Saves a new or existing Profile into the System (validation is part of the process )
	 *
	 * @param eCommerce_Entity_User_Address $address - The object to be saved
	 * @return eCommerce_Entity_User_Address         - The recently saved object
	 * @throws eCommerce_SDO_Core_Validator_Exception, SQLException
	 */
	public static function Save( eCommerce_Entity_User_Address $address ){
		
		// 1. Validate object
		$validator = new eCommerce_SDO_Core_Validator_UserAddress( $address );
		$validator->validate();
		
		if ( !$validator->isValid() ){
			throw new eCommerce_SDO_Core_Validator_Exception( $validator ); 
		}
		
		// 2. Save or Update Object
		$entity = new ArrayObject( $address );        // Convert Object to Array
		$dao = self::GetDAO();
		$dao->saveOrUpdate( $entity );
		
		// 3. Retrieve record from DB
		$address = self::LoadById( $entity[ 'profile_id' ] );
		if ( $address == new eCommerce_Entity_User_Address() ){
			throw new Exception( "The Address saved could not be retrieved." );      
		}
		// 4. Return recently saved object
		return $address;
		
	}
	
	/**
	 * Deletes the Address for the given User Profile ID.
	 *
	 * @param int $profileId                    - The ID of the profile who's address needs to be deleted
	 * @return eCommerce_Entity_Profile_Address - The recently deleted address
	 * @throws eCommerce_SDO_Core_Application_Exception - When the address is not found or couldn't be deleted
	 */
	public static function Delete( $profileId ){
		// 1. Ensure the address actually exists
		$address = self::LoadById( $profileId );
		
		if ( $address == new eCommerce_Entity_User_Profile() ){
			throw new eCommerce_SDO_Core_Application_Exception( 'The Profile Address to be deleted doesn\'t exist.' );
		}
		else {
			try {
				// 2. Delete the Category
				$dao = self::GetDAO();
				$dao->delete( $profileId );
				
				// 3. Return the recently deleted Category
				return $address;
			}
			catch ( SQLException $e ){
				throw new eCommerce_SDO_Core_Application_Exception( 'There was an unexpected error while trying to delete the Profile Address. See details for more information', 0, $e );
			}
		}
	}
	
	/**
	 * Retrieves the appropriate DAO
	 *
	 * @return eCommerce_SDO_Core_DAO_Address
	 */
	protected static function GetDAO(){
		return new eCommerce_SDO_Core_DAO_Address();
	}
	
}
?>