<?php
/**
 * This class provides access to manipulation of Users in the System.
 * 
 */	  
class eCommerce_SDO_Core_Application_Mailer extends eCommerce_SDO_Core_Application{
	protected $address;

	protected $body;
	protected $subject;
	function __construct(){
		parent::__construct();
		$this->address = array();
		$this->subject = '';
		$this->body ='';
	}
	

	public function setBody( $body ='' ){ $this->body = $body; }
	public function setSubject($subject = ''){$this->subject = $subject;}
	
	public function addAddress( $address, $name='', $type = 'TO' ){
		$this->address[] = array("email"=>$address,"name"=>$name, "type"=>$type);
	}
	
	public function resetAddress(){
		$this->address = array();
	}
	
	public function sendMails(){
		if(!empty($this->address)){
			$config = Config::getGlobalConfiguration();
		 	
			$mailer = new Mailer($config["smpt_mail_from"],$config["smpt_name_from"]);
		 	
		 	$mailer->setSmtp( $config["smpt_user"], $config["smpt_pwd"], true, $config["smpt_server"], $config["smpt_port_conection"]);
		 	$addresses = $this->address;
		 	foreach($addresses as $address){
		 		if(!empty($address["email"]) ){
		 			//Mailer::MAIL_DEST_TO
		 			//Mailer::MAIL_DEST_CC
		 			//Mailer::MAIL_DEST_BCC
		 			switch($address["type"]){
		 				case 'CC': $address["type"] = Mailer::MAIL_DEST_CC; break;
		 				case 'BCC': $address["type"] = Mailer::MAIL_DEST_BCC; break;
		 				default: $address["type"] = Mailer::MAIL_DEST_TO; break;
		 			}
		 			
					$mailer->addAddress( $address["email"], $address["name"], $address["type"]);
		 		}
		 	}		
			$mailer->setSubject( $this->subject );
			$mailer->setBody( $this->body , TRUE);
			$mailer->send();
		}	
	 }
	 	
	 
}

?>