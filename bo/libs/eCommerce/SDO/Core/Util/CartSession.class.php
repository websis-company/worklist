<?php
class eCommerce_SDO_Core_Util_CartSession {
	
	const NAMESPACES = 'cart';
	
	/**
	 * Saves the given Cart into the current session
	 *
	 * @param eCommerce_Entity_Cart $cart
	 */
	public static function SetCart( eCommerce_Entity_Cart $cart ){
		eCommerce_SDO_Core_Util_Session::Set( self::NAMESPACES, $cart );
	}
	
	public static function CartExists(){
		$tmp = eCommerce_SDO_Core_Util_Session::Get( self::NAMESPACES );
		if ( $tmp instanceof eCommerce_Entity_Cart ) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Retrieves the current Cart from the session
	 *
	 * @return eCommerce_Entity_Cart
	 */
	public static function GetCart(){
		if ( self::CartExists() ){
			return eCommerce_SDO_Core_Util_Session::Get( self::NAMESPACES );
		}
		else { 
			return new eCommerce_Entity_Cart();
		}
	}
	
	/**
	 * Destroys the Cart in the session
	 */
	public static function DestroyCart(){
		 eCommerce_SDO_Core_Util_Session::Delete( self::NAMESPACES );
	}
	
}
?>