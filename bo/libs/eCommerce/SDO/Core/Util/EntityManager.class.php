<?php
class eCommerce_SDO_Core_Util_EntityManager extends Object {
	
	public static function ParseArrayToObject( array $entity = null, eCommerce_Entity $object = null ){
		if ( $entity == null ){
			$entity = array();
		}
		foreach( $entity as $key => $value ){
			$function = 'set' . str_replace( ' ', '' , ucwords( str_replace( '_', ' ', $key ) ) );
			
			if ( method_exists( $object, $function ) ){
				$object->{$function}( $value );
			}
			else {
				throw new Exception( get_class( $object ) . " does not has a setter method for handling '$key': $function " );
			}
		}
		return $object;
	}
	
}
?>