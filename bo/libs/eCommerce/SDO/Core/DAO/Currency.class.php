<?php
class eCommerce_SDO_Core_DAO_Currency extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'currency';
	protected $id_field = 'currency_id';
	
	public function __construct(){
		parent::__construct();
		$this->setSearchFields( array( 
			'cu.currency_id', 'cu.name' 
			) );
	}

	/**
	 * @see DB_DAO_AdvancedDAO::getSqlFrom()
	 *
	 * @return unknown
	 */
	protected function getSqlFrom(){
		$sql = "FROM " . $this->table . " AS cu ";
		return $sql;
	}

	/**
	 * @see DB_DAO_AdvancedDAO::getSqlSelect()
	 *
	 * @return unknown
	 */
	protected function getSqlSelect(){
		return "SELECT cu.*";
	}

	/**
	 * @see DB_DAO_AdvancedDAO::getSqlGroupBy()
	 *
	 * @return unknown
	 */
	protected function getSqlGroupBy(){
		//return "GROUP BY p.product_id";
		return parent::getSqlGroupBy();
	}
}
?>