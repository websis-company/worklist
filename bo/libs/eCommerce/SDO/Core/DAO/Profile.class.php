<?php
class eCommerce_SDO_Core_DAO_Profile extends eCommerce_SDO_Core_DAO {
	
	protected $table = 'profile';
	protected $id_field = 'profile_id';
	//public $md5_fields = 'password';
	
	

	public function __construct(){
		parent::__construct( new eCommerce_SDO_Core_DB() );
		$this->setSearchFields( array( 'first_name', 'last_name', 'email', 'role' ) );
	}
	
	public function loadByEmail( $email ){
		$sql = "SELECT *"
		     . " FROM   " . $this->table
		     . " WHERE  email = '" . $this->db->escapeString( $email ) . "'";
		return $this->db->sqlGetRecord( $sql ); 
	}
	
	
}
?>