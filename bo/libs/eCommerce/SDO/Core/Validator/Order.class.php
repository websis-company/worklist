<?php
class eCommerce_SDO_Core_Validator_Order extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Order
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Order $order ){ 
		parent::__construct( $order );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupOrderId();
		$this->setupProfileId();
		$this->setupPaymentMethod();
		$this->setupStatus();
		$this->setupComments();
	}
	
	protected function setupOrderId(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		
		$msg[0] = "Order ID must be a positive integer value, zero or null";
		$msg[1] = "La ID de la &oacute;rden debe ser un entero positivo, cero o NULL";

		$tester = new Validator_Tester( $this->entity->getOrderId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  $msg[$lang] );
		$this->validator->addTester( 'order_id', $tester );
	}
	
	protected function setupProfileId(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		
		$msg[0] = "Profile ID must be a positive integer value, zero or null";
		$msg[1] = "La ID del perfil debe ser un entero positivo, cero o NULL";
		
		$msg1[0] = "The given Profile ID does not exist in the system.";
		$msg1[1] = "La ID de perfil seleccionada no existe en el sistema";
		
		$tester = new Validator_Tester( $this->entity->getProfileId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  $msg[$lang] );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( new eCommerce_SDO_Core_DAO_Profile() ),
		                  $msg1[$lang] );

		$this->validator->addTester( 'profile_id', $tester );
	}
	
	protected function setupPaymentMethod(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		
		$msg[0] = 'Payment method must be a valid string';
		$msg[1] = 'El m&eacute;todo de pago debe ser una cadena v&aacute;lida';
		
		$tester = new Validator_Tester( $this->entity->getPaymentMethod(), false );
		$tester->addTest( new Validator_Test_String(),
		                 $msg[$lang]  );
		$this->validator->addTester( 'payment_method', $tester );
	}
	
	protected function setupStatus(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		
		$msg[0] = 'Order status must be a valid string';
		$msg[1] = 'El estado de la &oacute;rden debe ser una cadena v&aacute;lida';
		
		$msg1[0] = 'Order status name must not be empty';
		$msg1[1] = 'El estado de la &oacute;rden no puede estar vac�&iacute;o';
		
		$tester = new Validator_Tester( $this->entity->getStatus(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  $msg1[$lang] );
		$tester->addTest( new Validator_Test_String(),
		                  $msg[$lang] );
		$this->validator->addTester( 'status', $tester );
	}
	
	protected function setupComments(){
		$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
		if ($actualLanguage == 'EN')
		{
			$lang = 0;
		}
		else
		{
			$lang = 1;
		}
		
		$msg[0] = 'Comments must be a valid string';
		$msg[1] = 'el campo comentario ser una cadena v&aacute;lida';
		
		$msg1[0] = 'HTML comments are not acepted';
		$msg1[1] = 'No se pueden aceptar comentarios en formato HTML';
		
		$tester = new Validator_Tester( $this->entity->getComments(), false );
		$tester->addTest( new Validator_Test_String(),
		                  $msg[$lang]);
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $msg1[$lang] );
		$this->validator->addTester( 'comments', $tester );
	}
	
}
?>