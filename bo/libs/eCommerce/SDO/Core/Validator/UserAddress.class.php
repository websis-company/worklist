<?php
class eCommerce_SDO_Core_Validator_UserAddress extends eCommerce_SDO_Core_Validator_Address {

	/**
	 * @var eCommerce_Entity_User_Address
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_User_Address $address ){ 
		parent::__construct( $address );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator_Address::setup()
	 *
	 */
	protected function setup(){
		parent::setup();
		$this->setupProfileId();
	}
	
	protected function setupProfileId(){
		$tester = new Validator_Tester( $this->entity->getProfileId(), true );
		
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( new eCommerce_SDO_Core_DAO_Profile() ),
		                  'Profile ID be from a valid user in the system.' );
		
		$this->validator->addTester( 'profile_id', $tester );
	}

}
?>