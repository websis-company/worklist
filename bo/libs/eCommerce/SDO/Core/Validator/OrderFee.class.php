<?php
class eCommerce_SDO_Core_Validator_OrderFee extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Order_Fee
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Order_Fee $order ){ 
		parent::__construct( $order );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupOrderFeeId();
		$this->setupOrderId();
		$this->setupDescription();
		$this->setupAmount();
	}
	
	protected function setupOrderFeeId(){
		$tester = new Validator_Tester( $this->entity->getOrderFeeId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Order Fee ID must be a positive integer value, zero or null' );
		$this->validator->addTester( 'order_fee_id', $tester );
	}
	
	protected function setupOrderId(){
		$tester = new Validator_Tester( $this->entity->getOrderId(), true );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Order ID must be a positive integer value, zero or null' );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( new eCommerce_SDO_Core_DAO_Order() ),
		                  'The Order ID given does not exist in the system.' );
		$this->validator->addTester( 'order_id', $tester );
	}

	protected function setupDescription(){
		$tester = new Validator_Tester( $this->entity->getDescription(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  'Fee description must not be empty' );
		$tester->addTest( new Validator_Test_String(),
		                  'Fee description must be a valid string' );
		$this->validator->addTester( 'description', $tester );
	}
	
	protected function setupAmount(){
		$tester = new Validator_Tester( $this->entity->getAmount(), true );
		$tester->addTest( new Validator_Test_Numeric(),
		                  'Amount must be a valid number' );
		$this->validator->addTester( 'amount', $tester );
	}
	
}
?>