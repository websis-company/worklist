<?php
class eCommerce_SDO_Core_Validator_Version extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_User_Profile
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Version $profile ){ 
		parent::__construct( $profile );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupName();
		$this->setupCurrency();
		$this->setupPrice();
		$this->setupSku();
		$this->setupStatus();

	}
	protected function setupSku(){
		$tester = new Validator_Tester( $this->entity->getSku(), false );
		$tester->addTest( new Validator_Test_NotEmpty(),
				$this->trans('sku').' '.$this->trans('is_required_msg') );
		$tester->addTest( new Validator_Test_String_NoHTML(),
				$this->trans('sku').' '.$this->trans('no_html_tags_msg') );
		$this->validator->addTester( 'sku', $tester );
	}
	
	protected function setupName(){
		$tester = new Validator_Tester( $this->entity->getName(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
				$this->trans('name').' '.$this->trans('is_required_msg') );
		$tester->addTest( new Validator_Test_String_NoHTML(),
				$this->trans('name').' '.$this->trans('no_html_tags_msg') );
		$this->validator->addTester( 'name', $tester );
	}
	
	protected function setupStatus(){
		$tester = new Validator_Tester( $this->entity->getStatus(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
				$this->trans('status').' '.$this->trans('is_required_msg') );
		$tester->addTest( new Validator_Test_String_NoHTML(),
				$this->trans('status').' '.$this->trans('no_html_tags_msg') );
		$this->validator->addTester( 'status', $tester );
	}
	
	protected function setupPrice(){
	
		$tester = new Validator_Tester( $this->entity->getPrice(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
				$this->trans('price').' '.$this->trans('is_required_msg') );
		$tester->addTest( new Validator_Test_Numeric_Unsigned(),
				$this->trans('price').' '.$this->trans('valid_monetary_qty_msg') );
		$this->validator->addTester( 'price', $tester );
	}
	
	protected function setupCurrency(){
		$tester = new Validator_Tester( $this->entity->getCurrency(), false );
	
		$tester->addTest( new Validator_Test_RegEx( '/^[A-Z]{3}$/'),
				$this->trans('currency').' '.str_replace('{n}', '3', $this->trans('n-character_code_msg')) );
		$this->validator->addTester( 'currency', $tester );
	}
	
	
}
?>