<?php
class eCommerce_SDO_Core_Validator_Product extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Catalog_Product
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Catalog_Product $product ){ 
		parent::__construct( $product );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		//$this->setupCurrency();
		//$this->setupDescriptionLong();
		//$this->setupDescriptionShort();
		//$this->setupDiscount();
		//$this->setupImageId();
		$this->setupName();
		//$this->setupPrice();
		$this->setupProductId();
		//$this->setupSku();
		$this->setupStatus();
		//$this->setupVolume();
		//$this->setupWeight();
	}
	
	protected function setupProductId(){
		$tester = new Validator_Tester( $this->entity->getProductId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Product ID must be a positive integer value, zero or null' );
		$this->validator->addTester( 'product_id', $tester );
	}
	
	protected function setupSku(){
		$tester = new Validator_Tester( $this->entity->getSku(), false );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  $this->trans('sku').' '.$this->trans('is_required_msg') );
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans('sku').' '.$this->trans('no_html_tags_msg') );
		$this->validator->addTester( 'sku', $tester );
	}
	
	protected function setupName(){
		$tester = new Validator_Tester( $this->entity->getName(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  $this->trans('name').' '.$this->trans('is_required_msg') );
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans('name').' '.$this->trans('no_html_tags_msg') );
		$this->validator->addTester( 'name', $tester );
	}
	
	protected function setupDescriptionShort(){
		$tester = new Validator_Tester( $this->entity->getDescriptionShort(), true );		
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  $this->trans('short_description').' '.$this->trans('is_required_msg') );
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans('short_description').' '.$this->trans('no_html_tags_msg') );				
		$this->validator->addTester( 'short_description', $tester );
		
	}
	
	protected function setupDescriptionLong(){
		$tester = new Validator_Tester( $this->entity->getDescriptionLong(), false );
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans('long_description').' '.$this->trans('no_html_tags_msg') );				
				
		$this->validator->addTester( 'long_description', $tester );
	}
	
	protected function setupStatus(){
		$tester = new Validator_Tester( $this->entity->getStatus(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  $this->trans('status').' '.$this->trans('is_required_msg') );
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans('status').' '.$this->trans('no_html_tags_msg') );				
				$this->validator->addTester( 'status', $tester );
	}
	
	protected function setupPrice(){
		
		$tester = new Validator_Tester( $this->entity->getPrice(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  $this->trans('price').' '.$this->trans('is_required_msg') );
		$tester->addTest( new Validator_Test_Numeric_Unsigned(),
		                  $this->trans('price').' '.$this->trans('valid_monetary_qty_msg') );				
		$this->validator->addTester( 'price', $tester );
	}

	protected function setupVolume(){
		$tester = new Validator_Tester( $this->entity->getVolume(), true );
		$tester->addTest( new Validator_Test_Numeric_Unsigned(),
		                  $this->trans('volume').' '.str_replace('{n}', '0', $this->trans('number_equal_greater_n_msg')) );				
		$this->validator->addTester( 'volume', $tester );
	}
	
	protected function setupCurrency(){
		$tester = new Validator_Tester( $this->entity->getCurrency(), false );
		
		$tester->addTest( new Validator_Test_RegEx( '/^[A-Z]{3}$/'),
		                  $this->trans('currency').' '.str_replace('{n}', '3', $this->trans('n-character_code_msg')) );				
		$this->validator->addTester( 'currency', $tester );
	}
	
	protected function setupDiscount(){
		$tester = new Validator_Tester( $this->entity->getDiscount(), false );
		$tester->addTest( new Validator_Test_Numeric(),
		                  $this->trans('discount').' '.$this->trans('valid_number_msg'));				
			
		$tester->addTest( new Validator_Test_Numeric_Between( 0, 100 ),
		                  $this->trans('discount').' '.str_replace(array('{m}','{n}'), array('0', '100'), $this->trans('number_between_m_and_n_msg')) );				
		$this->validator->addTester( 'discount', $tester );
	}
	
	protected function setupWeight(){
		$tester = new Validator_Tester( $this->entity->getWeight(), false );
		$tester->addTest( new Validator_Test_Numeric_Unsigned(),
		                  $this->trans('weight').' '.str_replace('{n}', '0', $this->trans('number_equal_greater_n_msg')) );
		$this->validator->addTester( 'weight', $tester );
	}
	
	
	protected function setupImageId(){
		$tester = new Validator_Tester( $this->entity->getImageId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Image ID must be a positive integer value, zero or null' );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( 
		                   new eCommerce_SDO_Core_DAO_File() ),
		                  'The Image ID provided doesn\'t exist.' );
		$this->validator->addTester( 'image_id', $tester );
	}
}
?>