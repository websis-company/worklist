<?php
class eCommerce_SDO_Core_Validator_Address extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Util_Address
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Util_Address $address ){ 
		parent::__construct( $address );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupStreet();
		$this->setupStreet2();
		$this->setupStreet3();
		$this->setupStreet4();
		$this->setupCity();
		$this->setupState();
		$this->setupZipCode();
		$this->setupCountry();
		$this->setupPhone();
		$this->setupPhone2();
		$this->setupPhone3();
		$this->setupPhone4();
//		$this->setupContactMethod();
	}
	
	protected function setupStreet(){
		$tester = new Validator_Tester( $this->entity->getStreet(), true );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                	$this->trans("address")." ".$this->trans("line")." #1 ".$this->trans("is_required_msg") );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("address")." ".$this->trans("line")." #1 ".$this->trans("no_html_tags_msg"));
		
		$this->validator->addTester( 'street', $tester );
	}
	
	protected function setupStreet2(){
		$tester = new Validator_Tester( $this->entity->getStreet2(), false );
		
//		$tester->addTest( new Validator_Test_NotEmpty(), 
//		                	$this->trans("address")." ".$this->trans("line")." #2 ".$this->trans("is_required_msg") );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("address")." ".$this->trans("line")." #2 ".$this->trans("no_html_tags_msg"));
		
		$this->validator->addTester( 'street2', $tester );
	}
		
	protected function setupStreet3(){
		$tester = new Validator_Tester( $this->entity->getStreet3(), false );
		
//		$tester->addTest( new Validator_Test_NotEmpty(), 
//		                	$this->trans("address")." ".$this->trans("line")." #3 ".$this->trans("is_required_msg") );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("address")." ".$this->trans("line")." #3 ".$this->trans("no_html_tags_msg"));
		
		$this->validator->addTester( 'street3', $tester );
	}
		
	protected function setupStreet4(){
		$tester = new Validator_Tester( $this->entity->getStreet4(), false );
		
//		$tester->addTest( new Validator_Test_NotEmpty(), 
//		                	$this->trans("address")." ".$this->trans("line")." #4 ".$this->trans("is_required_msg") );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("address")." ".$this->trans("line")." #4 ".$this->trans("no_html_tags_msg"));
		
		$this->validator->addTester( 'street4', $tester );
	}
	
	protected function setupCity(){
		$tester = new Validator_Tester( $this->entity->getCity(), false );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                	$this->trans("city")." ".$this->trans("is_required_msg(f)") );
				
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("city")." ".$this->trans("no_html_tags_msg"));
				
		$this->validator->addTester( 'city', $tester );
	}
	
	protected function setupState(){
		$tester = new Validator_Tester( $this->entity->getState(), false );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                	$this->trans("state")." ".$this->trans("is_required_msg") );
				
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("state")." ".$this->trans("no_html_tags_msg"));
				
		$this->validator->addTester( 'state', $tester );
	}
	
	protected function setupCountry(){
		$tester = new Validator_Tester( $this->entity->getCountry(), false );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                	$this->trans("country")." ".$this->trans("is_required_msg") );
				
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("country")." ".$this->trans("no_html_tags_msg") );
				
		$tester->addTest( new Validator_Test_Address_CountryCode(),
		                  $this->trans("country")." ".str_replace('{n}', '2', $this->trans("valid_{n}_letter_code_msg")) );
				
		$this->validator->addTester( 'country', $tester );
	}
	
	protected function setupZipCode(){
		$tester = new Validator_Tester( $this->entity->getZipCode(), false );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                	$this->trans("zip_code")." ".$this->trans("is_required_msg") );
				
		$tester->addTest( new Validator_Test_Address_ZipCode(), 
		                  $this->trans("zip_code")." ".str_replace('{n}', '5', $this->trans("valid_{n}_digit_code_msg")) );
		
		$this->validator->addTester( 'zip_code', $tester );
	}
	
	protected function setupPhone(){
		$tester = new Validator_Tester( $this->entity->getPhone(), false );
		
//		$tester->addTest( new Validator_Test_NotEmpty(), 
//		                	$this->trans("phone_number")." 1 ".$this->trans("is_required_msg") );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("phone_number")." 1 ".$this->trans("no_html_tags_msg"));
		
		$this->validator->addTester( 'phone', $tester );
	}
	
	protected function setupPhone2(){
		$tester = new Validator_Tester( $this->entity->getPhone2(), false );
		
//		$tester->addTest( new Validator_Test_NotEmpty(), 
//		                	$this->trans("phone_number")." 2 ".$this->trans("is_required_msg") );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("phone_number")." 2 ".$this->trans("no_html_tags_msg"));
		
		$this->validator->addTester( 'phone2', $tester );
	}
		
	protected function setupPhone3(){
		$tester = new Validator_Tester( $this->entity->getPhone3(), false );
		
//		$tester->addTest( new Validator_Test_NotEmpty(), 
//		                	$this->trans("phone_number")." 3 ".$this->trans("is_required_msg") );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("phone_number")." 3 ".$this->trans("no_html_tags_msg"));
		
		$this->validator->addTester( 'phone3', $tester );
	}
		
	protected function setupPhone4(){
		$tester = new Validator_Tester( $this->entity->getPhone4(), false );
		
//		$tester->addTest( new Validator_Test_NotEmpty(), 
//		                	$this->trans("phone_number")." 4 ".$this->trans("is_required_msg") );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans("phone_number")." 4 ".$this->trans("no_html_tags_msg"));
		
		$this->validator->addTester( 'phone4', $tester );
	}

	protected function setupContactMethod(){
		$contact_method = $this->entity->getContactMethod();
		$tester = new Validator_Tester( $contact_method, true );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                	$this->trans("contact_method")." ".$this->trans("is_required_msg") );
		
		$tester->addTest( new Validator_Test_RegEx('/^email|phone$/'),
		                  $this->trans("contact_method")." ".$this->trans("not_valid_msg").' (Email/'.$this->trans("phone_number").' '.strtolower($this->trans("required")).')');
		
		
		$phone  = trim($this->entity->getPhone());
		$phone2 = trim($this->entity->getPhone2());
		$phone3 = trim($this->entity->getPhone3());
		$phone4 = trim($this->entity->getPhone4());
		if($contact_method == 'phone' && empty($phone) && empty($phone2) && empty($phone3) && empty($phone4)) {
			$tester->addTest( new Validator_Test_AlwaysNotValid(),
												$this->trans('enter_phone_contact_method_msg'));	
		}
		
		$this->validator->addTester( 'contact_method', $tester );
	}
	
}
?>