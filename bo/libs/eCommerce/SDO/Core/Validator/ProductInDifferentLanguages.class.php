<?php
class eCommerce_SDO_Core_Validator_ProductInDifferentLanguages extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_Catalog_Product
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Catalog_ProductInDifferentLanguage $product ){ 
		parent::__construct( $product );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		/*
		$this->setupCurrency();
		$this->setupDescriptionLong();
		$this->setupDescriptionShort();
		$this->setupDiscount();
		$this->setupImageId();
		$this->setupName();
		$this->setupPrice();
		$this->setupProductId();
		$this->setupSku();
		$this->setupStatus();
		$this->setupVolume();
		$this->setupWeight();
		*/
	}
	
	protected function setupProductId(){
		$tester = new Validator_Tester( $this->entity->getProductId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Product ID must be a positive integer value, zero or null' );
		$this->validator->addTester( 'product_id', $tester );
	}
	
	protected function setupSku(){
		$tester = new Validator_Tester( $this->entity->getSku(), false );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  'SKU must not be empty' );
		$tester->addTest( new Validator_Test_String(),
		                  'SKU must be a valid string' );
		$this->validator->addTester( 'sku', $tester );
	}
	
	protected function setupName(){
		$tester = new Validator_Tester( $this->entity->getName(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  'Product name must not be empty' );
		$tester->addTest( new Validator_Test_String(),
		                  'Product name must be a valid string' );
		$this->validator->addTester( 'name', $tester );
	}
	
	protected function setupDescriptionShort(){
		$tester = new Validator_Tester( $this->entity->getDescriptionShort(), true );
		
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  'Product short description must not be empty' );
		
		$tester->addTest( new Validator_Test_String(),
		                  'Short description must be a valid string' );
		
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  'Short description must not be be HTML' );
		
		$this->validator->addTester( 'short_description', $tester );
		
	}
	
	protected function setupDescriptionLong(){
		$tester = new Validator_Tester( $this->entity->getDescriptionLong(), false );
		$tester->addTest( new Validator_Test_String(),
		                  'Long description must be a valid string' );
		
		$this->validator->addTester( 'long_description', $tester );
	}
	
	protected function setupStatus(){
		$tester = new Validator_Tester( $this->entity->getStatus(), true );
		$tester->addTest( new Validator_Test_NotEmpty(),
		                  'Status must not be empty' );
		$tester->addTest( new Validator_Test_String(),
		                  'Status must be a valid string' );
		$this->validator->addTester( 'status', $tester );
	}
	
	protected function setupPrice(){
		$tester = new Validator_Tester( $this->entity->getPrice(), true );
		$tester->addTest( new Validator_Test_Numeric_Unsigned(),
		                  'Price must be a valid unsigned number (integer or float)' );
		$this->validator->addTester( 'price', $tester );
	}

	protected function setupVolume(){
		$tester = new Validator_Tester( $this->entity->getVolume(), true );
		$tester->addTest( new Validator_Test_Numeric_Unsigned(),
		                  'Volume must be a valid unsigned number (integer or float)' );
		$this->validator->addTester( 'volume', $tester );
	}
	
	protected function setupCurrency(){
		$tester = new Validator_Tester( $this->entity->getCurrency(), false );
		
		$tester->addTest( new Validator_Test_RegEx( '/^[A-Z]{3}$/'),
		                  'Currency must be a valid 3-letter code (UPPERCASE).' );
		$this->validator->addTester( 'currency', $tester );
	}
	
	protected function setupDiscount(){
		$tester = new Validator_Tester( $this->entity->getDiscount(), false );
		$tester->addTest( new Validator_Test_Numeric(),
		                  'Discount must be a valid number.' );
		$tester->addTest( new Validator_Test_Numeric_Between( 0, 100 ),
		                  'Discount must be between 0 and 100.' );
		$this->validator->addTester( 'discount', $tester );
	}
	
	protected function setupWeight(){
		$tester = new Validator_Tester( $this->entity->getWeight(), false );
		$tester->addTest( new Validator_Test_Numeric_Unsigned(),
		                  'Weight must be a valid unsigned number (integer or float)' );
		$this->validator->addTester( 'weight', $tester );
	}
	
	
	protected function setupImageId(){
		$tester = new Validator_Tester( $this->entity->getImageId(), false );
		$tester->addTest( new Validator_Test_Numeric_UnsignedInteger(),
		                  'Image ID must be a positive integer value, zero or null' );
		$tester->addTest( new eCommerce_SDO_Core_Validator_Test_ExistingEntity( 
		                      new eCommerce_SDO_Core_DAO_File() ),
		                  'The Image ID provided doesn\'t exist.' );
		$this->validator->addTester( 'image_id', $tester );
	}
}
?>