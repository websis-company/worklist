<?php
class eCommerce_SDO_Core_Validator_Brand extends eCommerce_SDO_Core_Validator {
	
	/**
	 * @var eCommerce_Entity_User_Profile
	 */
	protected $entity;

	public function __construct( eCommerce_Entity_Brand $profile ){ 
		parent::__construct( $profile );
	}
	
	/**
	 * @see eCommerce_SDO_Core_Validator::setup()
	 */
	protected function setup(){
		$this->setupName();
	}
	
	protected function setupName(){
		$tester = new Validator_Tester( $this->entity->getName(), true );
		
		$tester->addTest( new Validator_Test_NotEmpty(), 
		                $this->trans('name').' '.$this->trans('is_required_msg') );
		
		// Verify first name is a valid string instead of anything else
		$tester->addTest( new Validator_Test_String_NoHTML(),
		                  $this->trans('name').' '.$this->trans('no_html_tags_msg') );
		
		$this->validator->addTester( 'name', $tester );
	}
	
}
?>