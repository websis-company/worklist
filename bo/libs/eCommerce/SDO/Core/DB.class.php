<?php
class eCommerce_SDO_Core_DB extends DB {

	public function __construct(){
		if ( SERVER_DEVELOPMENT ){
			$dsn = "mysql://root:@localhost/goplek";
		} 
		elseif ( SERVER_DEMO ){
			$dsn = "";
			//throw new eCommerce_SDO_Core_DB_Exception( 'DB Connection for DEMO server not defined yet.', 1 );
		} 
		elseif ( SERVER_PRODUCTION ){
			$dsn = "mysql://eshos_19725179:movdx09h@sql208.eshost.com.ar/eshos_19725179_goplek";
		} 
		else{
			$dsn = "";
		}

		parent::__construct( $dsn . "?new_link=true" );
		$this->db->setCharset( CHARSET_PROJECT );
	}

}
?>