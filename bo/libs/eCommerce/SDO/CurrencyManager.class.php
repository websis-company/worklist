<?php
class eCommerce_SDO_CurrencyManager {
	
	public static function GetCurrencies($arrOptions = false){
		return eCommerce_SDO_Core_Application_CurrencyManager::GetCurrencies($arrOptions);
	}
	
	public static function GetActualCurrency(){
		return eCommerce_SDO_Core_Application_CurrencyManager::GetActualCurrency( 'MXN' );
	}
	
	public static function ValueInActualCurrency( $mont, $currency ){
		
		return eCommerce_SDO_Core_Application_CurrencyManager::ValueInActualCurrency( $mont, $currency);
	}

	public function GetHTMLSelect( $selectName, $selected = '', $id = '', $CSSclass='', $selectParams ='' ){
		return eCommerce_SDO_Core_Application_CurrencyManager::GetHTMLSelect( $selectName, $selected, $id, $CSSclass, $selectParams );
	}
	
	public static function NumberFormat( $number ){
		return '$ ' . number_format($number,2,'.',',');
	}
	
	public static function SetActualCurrency($currency){
		eCommerce_SDO_Core_Application_CurrencyManager::SetActualCurrency($currency);
		
	}
}
?>