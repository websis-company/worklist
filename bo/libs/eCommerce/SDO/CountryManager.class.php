<?php
class eCommerce_SDO_CountryManager{
	public static function GetCountryById( $countryId ){
		return eCommerce_SDO_Core_Application_CountryManager::GetCountryById( $countryId );
	}
	
	public static function GetStatesOfCountry( $countryShortName, $htmlSelect = false, $selectName='', $selected = '', $id = '', $CSSclass='', $selectParams ='', $inAjax = false, $defaultText = '' ){
		return eCommerce_SDO_Core_Application_CountryManager::GetStatesOfCountry( $countryShortName, $htmlSelect, $selectName, $selected, $id, $CSSclass, $selectParams, $inAjax, $defaultText);
	}
	
	
	
	public static function GetStateById( $stateId, $countryId ){
		$state = eCommerce_SDO_Core_Application_CountryManager::GetStateById( $stateId, $countryId );
		if( empty($state) ){
			$state['name'] =$stateId;
		}
		return $state;
	}
}
?>