<?php
/**
 * This class contains default configuration options
 * 
 * @author 
 * @copyright 
 */
class Config extends Object {

	public static function Initialize(){
		self::SetConstants();
	}
	
	public static function getGlobalValue($value = 'project_name'){
		$config = self::getGlobalConfiguration();
		return $config[$value];
	}
	
	public static function getGlobalConfiguration(){		
		define('TEST_EMAIL', false);
		$config = array();
		
		if(SERVER_DEVELOPMENT){
			$config["ABS_HTTP_URL"] = "http://{$_SERVER['HTTP_HOST']}/goplek/worklist/";
		}elseif(SERVER_DEMO){
			$config["ABS_HTTP_URL"] = "http://{$_SERVER['HTTP_HOST']}/";
		}else{
			$config["ABS_HTTP_URL"] = "http://{$_SERVER['HTTP_HOST']}/worklist/";
		}
		
		$config["project_name"] 	= 'Lista de Tareas';
		$config["project_email"] 	= TEST_EMAIL ? '' : '';
		$config["email_root"] 		= TEST_EMAIL ? '' : '';

		$config["smpt_server"] 			= TEST_EMAIL ? 'vicomstudio.com' 			:	'';
		$config["smpt_user"] 			= TEST_EMAIL ? ''		:	'';
		$config["smpt_mail_from"] 		= TEST_EMAIL ? ''		:	'';
		$config["smpt_pwd"] 			= TEST_EMAIL ? 'password'					:	'password' 				 ;
		$config["smpt_port_conection"] 	= TEST_EMAIL ? 26	 						:	 25;
		$config["smpt_name_from"] 		= '';
		
		$config["project_domain"] 	= $_SERVER['HTTP_HOST'];
		$config["logo_project"] 	= ABS_HTTP_URL . "bo/backoffice/img/banner.jpg";
		$config["bakground_logo"] 	= "#FFFFFF";
		$config["color_th"] 		= "#3B2317";
		$config["color_td"] 		= "#008FD5";
		
		$config["color_login"] 		= "#000000";
		$config["bakground_logo"] 	= "#FFFFFF";
		$config["color_th"] 		= "#3B2317";
		$config["color_td"] 		= "#008FD5";
		
		$config["logo_email"]		= ABS_HTTP_URL . "bo/backoffice/img/banner.jpg";
		
		
		$config["paypal_test"] 				= true;
		$config["paypal_cpp_header_image"] 	= $config["ABS_HTTP_URL"] . "img/layout/banner.jpg";
		$config["paypal_input_image"] 		= $config["ABS_HTTP_URL"] . 'img/paypal.jpg';
		$config["paypal_notify_url"] 		= 'bo/paypal_ipn.php';
		$config["paypal_item_name"] 		= 'Pago de Pedido de Compra - Project';
		$config["paypal_cmd"] 				= '_xclick';
		$config["paypal_test_business"] 	= 'tienda_mx_01@vicomstudio.com';
		$config["paypal_business"] 			= 'ventas@ecobambu.com.mx';
		
		$config["deposito"]["beneficiario"] = '';
		$config["deposito"]["sucursal"] = 'Banco';
		$config["deposito"]["no_cuenta"] = 'xxxxxxxxxxxx';
		$config["transferencia"]["no_transf"] = 'xxxxxxxxxxxxxxxxxxxxxxxx';
		$config["deposito"]["clabe"] = 'xxxxxxxxxxxxxxxxxxxx';
		/*
		Access Code: 2C747464
		SecureSecret: 3BE6821E4F7240F440C857CB7086A2BB 
		*/
		/*if($_SERVER['REMOTE_ADDR']=="187.189.11.172"){
			$config["banamex_secureSecret"]		="3BE6821E4F7240F440C857CB7086A2BB" ;
			$config["banamex_vpc_AccessCode"]	= "2C747464" ;
			$config["banamex_vpc_Merchant"]		= "TEST1029460";
		}
		else{ */
			$config["banamex_secureSecret"]		="" ;
			$config["banamex_vpc_AccessCode"]	= "" ;
			$config["banamex_vpc_Merchant"]		= "";
		//}
		

		return $config;
	}
	
	protected  static function SetConstants(){
		
		if ( isset( $_SERVER['HTTP_HOST'] ) ){
			$serverDev =  self::IsServer( 'localhost', true )  
			           || self::IsServer( '127.0.0.1', true )
			           || self::IsServer( 'Admin2', true )
			|| self::IsServer( 'admin2', true );
		}
		else $serverDev = true;
		
		define( "SERVER_DEVELOPMENT", $serverDev );
		define( "SERVER_DEMO", $serverDemo);
		define( "SERVER_PRODUCTION", self::IsServer( 'arturohrdez.eshost.com.ar', true ) );
		
		if ( SERVER_DEVELOPMENT ){
			define('ABS_HTTP_URL', "http://{$_SERVER['HTTP_HOST']}/goplek/worklist/" );
			define('ABS_HTTP_PATH', "/goplek/worklist/" );
		}
		elseif ( SERVER_DEMO ){
			define('ABS_HTTP_URL', "http://{$_SERVER['HTTP_HOST']}/" );
			define('ABS_HTTP_PATH', "/" );
		}elseif ( SERVER_PRODUCTION ){
			define('ABS_HTTP_URL', "http://" . (Config::IsServer( "www.$_SERVER[HTTP_HOST].com", true ) ? ("www.") : "") . "$_SERVER[HTTP_HOST]/worklist/" );
			define('ABS_HTTP_PATH', "/worklist/" );
		}
	}

	public static function IsServer( $httpHost = 'localhost', $isEqual = true ){
		if ( strpos( $_SERVER[ 'HTTP_HOST'], $httpHost ) !== false ){
			return ( $isEqual ) ? true : false;
		}
		else {
			return ( $isEqual ) ? false : true;
		}
	}
	
	
	public function sendEmail( $subject, $message, $email, $html = true){
		$config = config::getGlobalConfiguration();
		
		$asunto=$subject;
		
		$header ="From: " . $config['project_name'] . "<" . $config['project_email'] . ">\n";
		
		
		
		if($html){
			$header .="Content-Type: text/html; charset=utf-8\n";
			$body ="<font style='font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#CC0000;font-weight:bold;'>". $config['project_name'] . "</font>\n" . $message;
		
			$body = '<p style="font-family:Arial;">' . nl2br($body) . '</p>';
		}else{
			$body =$message
			. "
			============================
			". $config['project_name'] ;
		} 
		$arrEmails = array();
		if( strpos($email, ',') ){
			$arrEmails = split(',',$arrEmails);
		}else{
			$arrEmails[] = $email;
			
		}
		$validator = new Validator_Test_Email();
		$result = count($arrEmails) > 0;
		foreach($arrEmails as $email){
			$email = strtolower($email);
			if( $validator->isValid($email) ){
				$result = $result && @mail($email, $asunto, $body, $header);
			}else{
				$result = false;
			}
		} 
		return $result;
	}
	public function sendAdminEmail( $subject, $message, $html = true){
		$config = config::getGlobalConfiguration();
		$email = $config["project_email"];
		
		return self::sendEmail( $subject, $message, $email, $html);
	}
	
}
?>