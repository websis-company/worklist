<?php
$users = $this->options->getResults();

$viewConfig = $this->viewConfig;

//params of the variable viewConfig
$paramsUsed = "name,title,id";
$paramsArrUsed = "columNamesOverride,hiddenColums,hiddenFields";

$paramsUsed = explode(",",$paramsUsed);

foreach( $paramsUsed as $param){
	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? '' : $viewConfig[ $param ];
}


$paramsArrUsed = explode(",",$paramsArrUsed);

foreach( $paramsArrUsed as $param){
	$viewConfig[ $param ] = empty( $viewConfig[ $param ] ) ? array() : $viewConfig[ $param ];
}


$data = array();
foreach($users as $user){
	$data[] = get_object_vars( $user );
}
		

$listPanel = new GUI_ListPanel_ListPanel( $viewConfig['name'] );
$listPanel->setData( $data );

foreach( $viewConfig['columNamesOverride'] as $colum => $rename){
	$listPanel->addColumnNameOverride($colum,$rename);
}


foreach( $viewConfig['hiddenColums'] as $col ){
	$listPanel->addHiddenColumn( $col );
}

foreach( $viewConfig['hiddenFields'] as $name=> $value ){
	$listPanel->addHiddenField($name, $value );
}

// Add "Order By" capabilities
$listPanel = new GUI_ListPanel_Decorator_Sorter( $listPanel );
$listPanel->setOrderBy( $this->options->getOrderBy() );


// Add actions
$listPanel = new GUI_ListPanel_Decorator_RowActions( $listPanel );
$listPanel->setHeaderLabel( $this->trans('actions') );

// View Works 
$actView = new GUI_ListPanel_Action('Tareas.php?action=view&listas_id='.$viewConfig['id'], 'img/chart_organisation.png' );
$actView->setImageTitle( 'Mostrar Tareas' );
//$actView->setOnClickEvent( 'editForm("edit","'.$viewConfig['id'].'")' );
$listPanel->addAction( $actView );

// Modify
//$actModify = new GUI_ListPanel_Action( '?cmd=edit&id=' . $viewConfig['id'] . '', 'img/write.png' );
$actModify = new GUI_ListPanel_Action('javascript:;', 'img/write.png' );
$actModify->setImageTitle( $this->trans('edit').' '.$viewConfig['name'] );
$actModify->setOnClickEvent( 'editForm("edit","'.$viewConfig['id'].'")' );
$listPanel->addAction( $actModify );

// Delete
$actDelete = new GUI_ListPanel_Action( '?cmd=delete&id=' . $viewConfig['id'] . '', 'img/delete.png' );
$actDelete->setImageTitle( $this->trans('delete').' '.$viewConfig['name'] );
$actDelete->setOnClickEvent( 'return confirm("'.$this->trans('?_confirm_delete').' '.strtolower($this->trans('the')).' '.$viewConfig['name'].' \"'.$viewConfig['id'].'\"?");' );
$listPanel->addAction( $actDelete );

// Add "Search" capabilities
$listPanel = new GUI_ListPanel_Decorator_Searcher( $listPanel );
$listPanel->setFieldLabel( '<img src="img/search2_small.png" style="vertical-align:middle">&nbsp;' );
$listPanel->setButtonLabel($this->trans('search'));
$listPanel->setClearButtonLabel($this->trans('clear'));
$listPanel->setSearchString( $this->options->getKeywords() );

// Add paging capabilities
$listPanel = new GUI_ListPanel_Decorator_Pager( $listPanel );
$listPanel->setCurrentPage( $this->options->getPage() );
$listPanel->setPagingLabel($this->trans('page'));
$listPanel->setPreviousLabel('&#9668; '.$this->trans('previous'));
$listPanel->setNextLabel($this->trans('next').' &#9658;');
$listPanel->setFirstPageLabel($this->trans('first(f)').' '.$this->trans('page'));
$listPanel->setLastPageLabel($this->trans('last(f)').' '.$this->trans('page'));
$listPanel->setOffsetLabel($this->trans('results_per').' '.strtolower($this->trans('page')));

//echo $this->totalPages;
$listPanel->setTotalPages( $this->options->getTotalPages() ); //TODO:

//$listPanel->setOffsetIncrement( $this->options->getResultsPerPage() );
$listPanel->setOffset( $this->options->getResultsPerPage() );

include_once ( "includes/header.php" );
?>


<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> Listas de Tareas</h1>
		<!-- <ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Tables</a></li>
			<li class="active">Data tables</li>
		</ol> -->
	</section>

    <div id="divEditForm" class="col-xs-12 col-md-8 col-md-offset-2" style="display: none;"></div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        	<div class="box" style="overflow-x: scroll;">
        		<div class="box-header">
                    <div class="cleafix"></div>

        			<div class="box-title pull-right">
        				<!--<a href="?cmd=add" class="btn btn-default">
	        				<img src="img/wand.png" border="0" hspace="2" align="absmiddle">
	        				<?=$this->trans('add').' '.$this->trans('new').' '.$this->trans('user')?>
        				</a>-->
                        <div class="loading"></div>
                        <a id="btnEditForm" class="btn btn-default" onclick="editForm('new');" style="cursor: pointer;">
                            <img src="img/wand.png" border="0" hspace="2" align="absmiddle">
                            <?php echo "Agregar Lista de Tareas";?>
                        </a>
                    </div>
        		</div><!-- /.box-header -->
        		<div class="box-body">
        		<?php
        			$listPanel->setTableWidth( '100%' );
        			$listPanel->display();
        		?>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->	
</div><!-- /.content-wrapper -->

<?php
include_once ( "includes/footer.php" );
?>

<script type="text/javascript">
	$(function () {
        $("#table").DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "order": [[ 2, "desc" ]]
        });
    });

    function editForm(type,id){
        if(type == 'new'){
            cmd = 'cmd=add';
        }else if(type == 'edit'){
            cmd = 'cmd=edit&id='+id;
        }//end if

        var Url = "<?php echo ABS_HTTP_URL; ?>";
        $.ajax({
            'type'    :"GET",
            'url'     : Url+'bo/backoffice/Listas.php',
            'data'    : cmd,
            'dataType': "html",
            beforeSend: function(data){
                $("#btnEditForm").attr('disabled',true);
                $(".loading").html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
            },
            success:  function (data){
                $("#btnEditForm").hide();
                $(".loading").hide();
                $("#divEditForm").html(data);
                $("#divEditForm").slideDown(function(e){});

            }
        });
    }

</script>