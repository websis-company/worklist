<?php
//include_once ( "header.php" );
$form          = $this->form;
$viewConfig    = $this->viewConfig;
$errors        = $this->errors;
$ArrayImages   = $this->ArrayImages;
$cancelParams  = empty($this->cancelParams) ? '' : $this->cancelParams;
$cancelConfirm = empty($this->cancelConfirm) ? '' : $this->cancelConfirm;

$object = $this->object;
?>

<div class="row">
   <!-- left column -->
   <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-success">
         <div class="box-header with-border">
            <h3 class="box-title"><? echo $viewConfig['title']; ?></h3>
         </div><!-- /.box-header -->
         <!-- form start -->
        <form name="frmListas" id="frmListas" method="post" action="" enctype="multipart/form-data">
        	<div class="box-body">
        		<div class="form-group">
        			<label for="name">Nombre :</label>
        			<input type="text" class="form-control" id="name" name="name" placeholder="Nombre" value="<?php echo $object->getName(); ?>">
        		</div>
        		<div class="form-group">
        			<label for="name">Descripciòn :</label>
        			<textarea class="form-control" id="name" name="description" rows="3" placeholder="Descripciòn"><?php echo $object->getDescription(); ?></textarea>
        		</div>
        		<!--<div class="form-group">
        			<label>Fecha Inicio - Fin:</label>
        			<div class="input-group">
        				<div class="input-group-addon">
        					<i class="fa fa-clock-o"></i>
        				</div>
        				<?php 
        					/*$validRange = $object->getStartdate();
        					if(empty($validRange)){
        						$range = '';
        					}else{
								    $startdate = date('m/d/Y g:i A',strtotime($object->getStartdate()));
								    $enddate   = date('m/d/Y g:i A',strtotime($object->getEnddate()));
								    $range     = $startdate.' - '.$enddate;
        					}*/
        				?>
        				<input type="text" class="form-control pull-right" id="rangeTime" name="rangeTime" value="<?php echo $range; ?>">
        			</div>
        		</div>-->
        		<div class="form-group">
        			<label>Es importante ? </label>
    				<select class="form-control" name="important" id="important">
    					<option value="No" <?php echo ($object->getImportante() == "No") ? 'selected="selected"' : '' ; ?>>No</option>
    					<option value="Si" <?php echo ($object->getImportante() == "Si") ? 'selected="selected"' : '' ; ?>>Si</option>
              		</select>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
            	<input type="hidden" name="cmd" value="save">
            	<input type="hidden" name="listas_id" value="<?php echo $object->getListasId(); ?>">
            	<button type="submit" class="btn btn-primary pull-right" style="margin-left: 15px;">Aceptar</button>
            	<button type="button" class="btn btn-default pull-right" onClick="closeForm();">Cancelar</button>
            </div>
         </form>
      </div><!-- /.box -->
   </div>
</div>


<script type="text/javascript">
	function closeForm(){
		$("#divEditForm").slideUp();
		$("#frmListas")[0].reset();
		$("#btnEditForm").show(function(e){
			$("#btnEditForm").attr('disabled',false);
			$(".loading").html('');
			$(".loading").show('');
		});
	}// end function

	$(document).ready(function(e){
		$("#frmListas").validate({
			debug: false,
			rules: {
				name: {required: true,},
			}
		});
		//Date range picker with time picker
        $('#rangeTime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
	});

</script>