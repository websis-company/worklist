<?php
//include_once ( "header.php" );

$form          = $this->form;
$viewConfig    = $this->viewConfig;
$errors        = $this->errors;
$ArrayImages   = $this->ArrayImages;
$cancelParams  = empty($this->cancelParams) ? '' : $this->cancelParams;
$cancelConfirm = empty($this->cancelConfirm) ? '' : $this->cancelConfirm;
$object = $this->object;
?>

<div class="row">
	<!-- left column -->
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title"><? echo $viewConfig['title']; ?></h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form name="frmTareas" id="frmTareas" method="post" action="" enctype="multipart/form-data">
				<div class="box-body">
					<div class="form-group">
						<label for="listas_id">Relacionar a Lista ?:</label>
						<?php $listas = eCommerce_SDO_Core_Application_Listas::GetAll(); ?>
						<select class="form-control select2" name="listas_id" id="listas_id" style="width: 100%;">
							<option value="">No</option>
							<?php 
							foreach ($listas as $lista_item) {
							?>
							<option value="<?php echo $lista_item->getListasId(); ?>" <?php echo ($object->getListasId() == $lista_item->getListasId()) ? 'selected="selected"': "" ; ?>><?php echo $lista_item->getName();?></option>
							<?php
							}//end foreach
							?>
						</select>
					</div>

					<div class="form-group">
						<label for="name">Nombre :</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="Nombre" value="<?php echo $object->getName(); ?>">
					</div>
					<div class="form-group">
						<label for="name">Descripciòn :</label>
						<textarea class="form-control" id="name" name="description" rows="3" placeholder="Descripciòn"><?php echo $object->getDescription(); ?></textarea>
					</div>
					<div class="form-group">
						<label>Fecha Inicio - Fin :</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-clock-o"></i>
							</div>
							<?php 
							$validRange = $object->getStartdate();
							if(empty($validRange)){
								$range = '';
							}else{
								$startdate = date('m/d/Y g:i A',strtotime($object->getStartdate()));
								$enddate   = date('m/d/Y g:i A',strtotime($object->getEnddate()));
								$range     = $startdate.' - '.$enddate;
							}
							?>
							<input type="text" class="form-control pull-right" id="rangeTime" name="rangeTime" value="<?php echo $range; ?>" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label>Tarea Terminada ?</label> <br/>
						Si <input type="radio" id="status" name="status" value="Terminado" class="flat-red" <?php echo ($object->getStatus() == "Terminado") ? "checked": ""; ?>>
						No <input type="radio" id="status" name="status" value="Activo" class="flat-red" <?php echo ($object->getStatus() == "Activo") ? "checked" :"";?>>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<input type="hidden" name="cmd" value="save">
					<input type="hidden" name="tareas_id" value="<?php echo $object->getTareasId(); ?>">
					<button type="submit" class="btn btn-primary pull-right" style="margin-left: 15px;">Aceptar</button>
					<button type="button" class="btn btn-default pull-right" onClick="closeForm();">Cancelar</button>
				</div>
			</form>
		</div><!-- /.box -->
	</div>
</div>


<script type="text/javascript">
	function closeForm(){
		$("#divEditForm").slideUp();
		$("#frmTareas")[0].reset();
		$("#btnEditForm").show(function(e){
			$("#btnEditForm").attr('disabled',false);
			$(".loading").html('');
			$(".loading").show('');
		});
	}// end function

	$(document).ready(function(e){
		$("#frmTareas").validate({
			debug: false,
			rules: {
				name: {required: true,},
			}
		});

		$(".select2").select2();
		//Date range picker with time picker
		$('#rangeTime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
		//Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });
	});

</script>



<!--
  <link rel="stylesheet" type="text/css" media="all" href="calendar/css/jscal2.css" title="jscal2" />

  <script type="text/javascript" src="calendar/js/jscal2.js"></script>

  <script type="text/javascript" src="calendar/js/lang/<?=eCommerce_SDO_LanguageManager::LanguageToZendLang()?>.js"></script>
  
<h2 align="left" style="margin:0px;"><? echo $viewConfig['title']; ?></h2>

<form name="frmUser" id="frmUser" method="post" action="" enctype="multipart/form-data">

  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div><br />
  <?php $errors->getHtmlError("generalError"); ?>
	

		<?php
			eCommerce_SDO_Core_Application_Form::displayForm( $form, $errors,'frmUser' );
		
		?>

<p align='left'>
	<input type="hidden" name="cmd" value="save">
	
	<input type="submit" value="<?=$this->trans('save')?>" class="frmButton"> <input type="button" value="<?=$this->trans('cancel')?>" class="frmButton" onClick="<?php echo $cancelConfirm; ?>document.location.href='Tareas.php<?php echo $cancelParams; ?>'">
</p>
</form>-->

<?php
//include_once ( "footer.php" );
?>