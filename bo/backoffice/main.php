<?php 
eCommerce_FrontEnd_BO_Authentification::verifyAuthentification();
include_once( "includes/header.php" );
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <?//=$config["project_name"];?>Control panel</h1>
    <ol class="breadcrumb">
      <li>
        <a href="<?php echo ABS_HTTP_URL;?>bo/backoffice/index.php">
          <i class="fa fa-home"></i> Home
        </a>
      </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
    <?php
      $colors = array('1' => 'bg-yellow','2' => 'bg-aqua','3' => 'bg-green','4'=>'bg-red','5'=>'bg-blue','6'=>'bg-orange','7'=>'bg-purple','8'=>'bg-gray');
      $i = 1;
      foreach ($sections as $key => $section) {
        if($section["include"]){
    ?>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box <?php echo $colors[$i];?>">
          <div class="inner">
            <h3>&nbsp;</h3>
            <p><h4><?=$section["name"];?></h4></p>
          </div>
          <div class="icon">
            <i class="fa <?=$section["icon"];?>"></i>
          </div>
          <a href="<?=$key?>.php" class="small-box-footer">M&aacute;s Inforaci&oacute;n <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->          
    <?php
        $i++;
        if($i==9){$i=1;}
        }//end if
      }//end foreach
    ?>
                     

    </div><!-- /.row -->
  </section><!-- /.content -->      
</div><!-- /.content-wrapper -->

<?php include_once( "includes/footer.php" ); ?>