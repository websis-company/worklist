<?php
include_once ( "header.php" );

function dataToInput( $value ){
	return htmlentities( $value, ENT_QUOTES, CHARSET_PROJECT );
}

$errors = $this->errors;
$address = $this->address;
//$address = new eCommerce_Entity_User_Address();

$this->display( 'user/info.php' );
?>


<h2 align="left" style="margin:12px 0px;"><?=$this->trans('edit').' '.$this->trans('address')?></h2>

<form name="frmUser" id="frmUser" method="post" action="user.php" onsubmit="">

  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div>
	
	<fieldset>
		<legend><?=$this->trans('information')?></legend>
		
		<dl class="form">
			<dt><?=$this->trans('street').' '.$this->trans('and').' '.$this->trans('number')?>:*</dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="frmInput"
				       name="entity[street]" id="street"
				       value="<?php echo dataToInput( $address->getStreet()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street"); ?>
			
			<dt><?=$this->trans('residential_development')?>: </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="frmInput"
				       name="entity[street2]" id="street2"
				       value="<?php echo dataToInput( $address->getStreet2()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street2"); ?>
<!-- 
			<dt><?=$this->trans('street')?> 3: </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="frmInput"
				       name="entity[street3]" id="street3"
				       value="<?php echo dataToInput( $address->getStreet3()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street3"); ?>
			
			<dt><?=$this->trans('street')?> 4: </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="frmInput"
				       name="entity[street4]" id="street4"
				       value="<?php echo dataToInput( $address->getStreet4()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street4"); ?>
-->			
			<dt><?=$this->trans('zip_code')?>: *</dt>
			<dd>
				<input type="text" size="5" maxlength="5" class="frmInput"
				       name="entity[zip_code]" id="zip_code"
				       value="<?php echo dataToInput( $address->getZipCode()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("zip_code"); ?>
			
			<dt><?=$this->trans('city')?>: *</dt>
			<dd>
				<input type="text" size="30" maxlength="30" class="frmInput"
				       name="entity[city]" id="city"
				       value="<?php echo dataToInput( $address->getCity()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("city"); ?>
			
			<dt><?=$this->trans('country')?>: *</dt>
			<dd>
			<?php
					echo eCommerce_SDO_Core_Application_CountryManager::GetHTMLSelect( 'entity[country]', $address->getCountry(), 'country', 'frmInput' , 'onChange="_refreshSTATE( this.value )"');
			?>
				<?php
		//			echo eCommerce_SDO_Core_Application_CountryManager::GetHTMLSelect( 'entity[country]', $address->getCountry(), 'country', 'frmInput' , 'onChange="alert(\'\'+this.value);"');
				?>
			</dd>
			<?php $errors->getHtmlError("country"); ?>
			
			<dt><?=$this->trans('state')?>: *</dt>
			<dd>
				<?php
					//$states = eCommerce_SDO_CountryManager::GetStatesOfCountry( $address->getCountry(), true, 'entity[state]', $address->getState(),'state','frmInput', '' );
					//echo $states; 
				?>
				<?php

					$states = eCommerce_SDO_CountryManager::GetStatesOfCountry( $address->getCountry(), true, 'entity[state]', $address->getState(),'state','frmInput', '', true,'../' );
					echo ( !is_array($states)) ? $states : $states["div"];
			?>	
			</dd>
			<?php $errors->getHtmlError("state"); ?>
			
			
			
			
			
		</dl>
		
	</fieldset>
	<input type="hidden" name="entity[profile_id]" value="<?php echo $address->getProfileId() ?>">
	<input type="hidden" name="cmd" value="saveAddress" >
	
	<input type="submit" value="<?=$this->trans('save')?>" class="frmButton">
	<input type="button" value="<?=$this->trans('cancel')?>" class="frmButton" onclick="window.location.href='user.php'">
</form>
<?php
echo ( !is_array($states)) ? '' : $states["formAndAjaxScript"];
?>

<?php
include_once ( "footer.php" );
?>