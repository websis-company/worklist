<?php
include_once ( "header.php" );

function dataToInput( $value ){	
		return htmlentities( $value, ENT_QUOTES, CHARSET_PROJECT );
}

$errors = $this->errors;
	
$user = $this->user;
$ArrRoles = $this->ArrRoles;
$ArrRoles = array_combine( $ArrRoles, $ArrRoles );
?>

<h2 align="left" style="margin:0px;"><?php echo $this->strSubtitles; ?></h2>

<form name="frmUser" id="frmUser" method="post" action="user.php" onsubmit="">

  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div>
	
	<fieldset>
		<legend>Account Information</legend>
		
		<dl class="form">
			<dt>Email: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[email]" id="email"
				       value="<?php echo dataToInput($user->getEmail());?>" >
			</dd>
			<?php $errors->getHtmlError("email"); ?>
			
			<dt>Password: *</dt>
			<dd>
				<input type="password" size="30" maxlength="100" class="frmInput"
				       name="entity[password]" id="password"
				       value="<?php echo dataToInput(trim($user->getPassword()));?>">
			</dd>
			<?php $errors->getHtmlError("password"); ?>
			
			<dt>Role: *</dt>
			<dd>
				<?php
					Util_HTML::CreateDropDown( 'entity[role]', 'role', $ArrRoles, $user->getRole(), 'frmInput' );
				?>
			</dd>
			<?php $errors->getHtmlError("role"); ?>
		</dl>
	</fieldset>
	
	<fieldset>
		<legend>Personal Information</legend>
		
		<dl class="form">
			<dt>First Name: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[first_name]" id="first_name"
				       value="<?php echo dataToInput( $user->getFirstName()  ) ?>">
			</dd>
			<?php $errors->getHtmlError("first_name"); ?>

			<dt>Las Name: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[last_name]" id="last_name"
				       value="<?php echo dataToInput( $user->getLastName()  ) ?>">
			</dd>
			<?php $errors->getHtmlError("last_name"); ?>
			
		</dl>
		
	</fieldset>
	<input type="hidden" name="cmd" value="save">
	<input type="hidden" name="entity[id]" value="<?php echo $user->getProfileId()?>">
	<input type="hidden" name="entity[confirmado]" value="<?php echo $user->getConfirmado()?>">
	
	<input type="submit" value="<?=$this->trans('save')?>" class="frmButton">
	<input type="button" value="<?=$this->trans('cancel')?>" class="frmButton" onclick="window.location.href='user.php'">
</form>

<?php
include_once ( "footer.php" );
?>