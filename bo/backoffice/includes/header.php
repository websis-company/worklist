<!DOCTYPE html>
<?php 
date_default_timezone_set("America/Mexico_City");
require_once( "../common.php" );

$lang   = eCommerce_SDO_Core_Application_LanguageManager::GetActualLanguage();
$config = Config::getGlobalConfiguration();

// secciones del BackOffice
$sections               = array();
//$sections["index"]      = array("include" => 1, "name" => $this->trans('home'));
//$sections["user"]       = array("include" => 1, "name" => $this->trans('user').'s','icon'=>'fa-users');
$sections["Tareas"]       = array("include" => 1, "name" => "Tareas",'icon'=>'fa fa-list-ol');
$sections["Listas"]       = array("include" => 1, "name" => "Listas",'icon'=>'fa fa-list-alt');
$sections["Today"]       = array("include" => 1, "name" => "Hoy",'icon'=>'fa fa-calendar-plus-o');
$sections["Pending"]       = array("include" => 1, "name" => "Pendientes",'icon'=>'fa fa-calendar-minus-o');
$sections["Finish"]       = array("include" => 1, "name" => "Terminadas",'icon'=>'fa fa-calendar-check-o');

/*$sections["category"]   = array("include" => 1, "name" => "Categor&iacute;as",'icon'=>'fa-sitemap');
$sections["product"]    = array("include" => 1, "name" => 'Productos','icon'=>'fa-list-alt');
$sections["Brand"]      = array("include" => 1, "name" => $this->trans('brand').'s','icon'=>'fa-registered');
$sections["order"]      = array("include" => 1, "name" => "&Oacute;rdenes",'icon'=>'fa-credit-card');
$sections["Cupones"]    = array("include" => 1, "name" => "Cupones",'icon'=>'fa-ticket');  
$sections["Reportes"]   = array("include" => 1, "name" => "Reportes",'icon'=>'fa-file-text-o');*/
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?=$config["project_name"]?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/datatables/dataTables.bootstrap.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/dist/css/skins/_all-skins.min.css">	
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/iCheck/square/blue.css">
	<!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/iCheck/all.css">
	<!-- Morris chart -->
	<!-- <link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/morris/morris.css"> -->
	<!-- jvectormap -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/datepicker/datepicker3.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/daterangepicker/daterangepicker-bs3.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/select2/select2.min.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			.error{color: #b94a48; font-size: 13px; font-family: Arial;}
			input.error, input.error:focus, input.check.error{border: 1px solid #b94a48 !important;}
			textarea.error, textarea.error:focus, input.check.error{border: 1px solid #b94a48 !important;}
		</style>
		<script src="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/validate/jquery.validate.js"></script>
		<script type="text/javascript" src="<?php echo ABS_HTTP_URL;?>bo/backoffice/includes/plugins/validate/dist/localization/messages_es.js"></script>
	</head>

	<?php 
	if( eCommerce_FrontEnd_BO_Authentification::verifyAuthentification(false) ){
		$userProfile = eCommerce_FrontEnd_Util_UserSession::GetIdentity();
		?>
		<body class="hold-transition skin-green sidebar-mini">
			<div class="wrapper">
				<header class="main-header">
					<!-- Logo -->
					<a href="<?php echo ABS_HTTP_URL ?>bo/backoffice/index.php" class="logo">
						<!-- mini logo for sidebar mini 50x50 pixels -->
						<span class="logo-mini"><?php echo $config["project_name"]; ?></span>
						<!-- logo for regular state and mobile devices -->
						<span class="logo-lg"><?php echo $config["project_name"]; ?></span>
					</a>
					<!-- Header Navbar: style can be found in header.less -->
					<nav class="navbar navbar-static-top" role="navigation">
						<!-- Sidebar toggle button-->
						<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
							<span class="sr-only">Toggle navigation</span>
						</a>
						<div class="navbar-custom-menu">
							<ul class="nav navbar-nav">
								<!-- Notifications: Important List -->
								<li>
									<?php 
									$extra = "importante = 'Si'";
									$list = eCommerce_SDO_Core_Application_Listas::GetAllInArray($extra);
									$total = count($list);
									?>
									<a href="Listas.php?action=viewImportant" title="Lista de tareas importantes">
										<i class="fa fa fa-list-alt" aria-hidden="true"></i>
										<span class="label label-danger"><?php echo $total; ?></span>
									</a>
								</li>

								<!-- Notifications: Works Today -->
								<li class="dropdown notifications-menu">
									<?php 
									$extra = "startdate LIKE '%".date('Y-m-d')."%'";
									$works = eCommerce_SDO_Core_Application_Tareas::GetAllInArray($extra);
									$total = count($works);
									?>
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Tareas para Hoy">
										<i class="fa fa-calendar-plus-o"></i>
										<span class="label label-primary"><?php echo $total; ?></span>
									</a>
									<ul class="dropdown-menu">
									<li class="header">Tienes <?php echo $total; ?> Tareas para hoy</li>
										<li>
											<!-- inner menu: contains the actual data -->
											<ul class="menu">
												<?php 
												foreach ($works as $works_item) {
												?>
												<li>
													<a href="javascript:;">
														<?php echo $works_item; ?>
													</a>
												</li>
												<?php
												}//end foreach
												?>
												
											</ul>
										</li>
										<li class="footer"><a href="<?php echo ABS_HTTP_URL ?>bo/backoffice/Today.php">View all</a></li>
									</ul>
								</li>

								<!-- Notifications: Works Pending-->
								<li class="dropdown notifications-menu">
									<?php 
									$extra = "enddate < '".date('Y-m-d')."' && status != 'Terminado'";
									$works = eCommerce_SDO_Core_Application_Tareas::GetAllInArray($extra);
									$total = count($works);
									?>
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Tareas Pendientes">
										<i class="fa fa-calendar-minus-o"></i>
										<span class="label label-warning"><?php echo $total; ?></span>
									</a>
									<ul class="dropdown-menu">
									<li class="header">Tienes <?php echo $total; ?> Tareas Pendientes</li>
										<li>
											<!-- inner menu: contains the actual data -->
											<ul class="menu">
												<?php 
												foreach ($works as $works_item) {
												?>
												<li>
													<a href="javascript:;">
														<?php echo $works_item; ?>
													</a>
												</li>
												<?php
												}//end foreach
												?>
												
											</ul>
										</li>
										<li class="footer"><a href="<?php echo ABS_HTTP_URL ?>bo/backoffice/Pending.php">View all</a></li>
									</ul>
								</li>
								<!-- User Account: style can be found in dropdown.less -->
								<li class="user user-menu">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<img src="<?php echo ABS_HTTP_URL; ?>bo/backoffice/includes/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
										<span class="hidden-xs">Bienvenido <strong><?php echo $userProfile->getFirstName(); ?></strong></span>
									</a>
								</li>
								<!-- Control Sidebar Toggle Button -->
								<li>
									<a href="<?php echo ABS_HTTP_URL;?>bo/backoffice/index.php?cmd=logout" title="Cerrar Sesi&oacute;n"><i class="fa fa-sign-out"></i></a>
								</li>
							</ul>  
						</nav>
					</header>
					<!-- Left side column. contains the logo and sidebar -->
					<aside class="main-sidebar">
						<!-- sidebar: style can be found in sidebar.less -->
						<section class="sidebar">
							<!-- sidebar menu: : style can be found in sidebar.less -->
							<ul class="sidebar-menu">
								<li class="header" style="text-align: center; color: #FFF; font-weight: bold;">
									<h5>MENÚ PRINCIPAL</h5>
								</li>

								<?php
								foreach ($sections as $key => $section) {
									if($section["include"]){
										$active = $key.'.php' == Util_Server::getScript() ? 'active' : '';
										?>
										<li class="<?php echo $active;?> treeview">
											<a href="<?=$key?>.php">
												<i class="fa <?=$section["icon"];?>"></i>
												<span><?=$section["name"];?></span><i class="fa fa-angle-right pull-right"></i>
											</a>
										</li>
										<?php
			          }//end if
			        }//end foreach
			        ?>
			    </ul>
			</section>
			<!-- /.sidebar -->
		</aside>			
		<?php 
	}else{
		?>
		<body class="hold-transition login-page">
			<div class="login-box">	
				<?php
	}//end if
	?>