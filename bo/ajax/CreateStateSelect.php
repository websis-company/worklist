<?php
@require_once("../common.php");

$selectName = $_REQUEST["selectName"];
$selectedCountry = $_REQUEST["selectedCountry"];

$id = $_REQUEST["id"];
$CSSclass = $_REQUEST["CSSclass"];
$defaultText = $_REQUEST["defaultText"];

$selectName = empty($selectName) ? 'entity[estado]' : $selectName;
$selectedCountry = empty($selectedCountry) ? 'MX' : $selectedCountry;
$id = empty($id) ? 'estado' : $id;

switch($selectedCountry) {
	case 'MX' :
		echo Util_State_MXState::GetHTMLSelect( $selectName, '', $id, $CSSclass, '', false, $defaultText );
		break;
	case 'US' :
		echo Util_State_USState::GetHTMLSelect( $selectName, '', $id, $CSSclass, '', false, $defaultText );
		break;
	default :
		echo '<input type="text" size="30" name="'.$selectName.'" id="'.$id.'" value="" />';
}
?>