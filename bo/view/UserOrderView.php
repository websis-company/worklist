<?php
include_once ( "headerFlujo.php" );
?>


<link rel="stylesheet" type="text/css" href="<?=ABS_HTTP_URL?>css/_styles.css" media="screen" />

<style>
	@font-face {
	    font-family: 'lemonyellowsunuploaded_file';
	    src: url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.eot');
	    src: url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.eot?#iefix') format('embedded-opentype'),
	         url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.woff2') format('woff2'),
	         url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.woff') format('woff'),
	         url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.ttf') format('truetype'),
	         url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.svg#lemonyellowsunuploaded_file') format('svg');
	    font-weight: normal;
	    font-style: normal;
	}



	@font-face {
	    font-family: 'mf_really_awesomeregular';
	    src: url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.eot');
	    src: url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.eot?#iefix') format('embedded-opentype'),
	         url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.woff2') format('woff2'),
	         url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.woff') format('woff'),
	         url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.ttf') format('truetype'),
	         url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.svg#mf_really_awesomeregular') format('svg');
	    font-weight: normal;
	    font-style: normal;
	}

</style>

<style type="text/css">
	/* #E03030 */
	.error{color: #dd4b39 !important; font-family: Arial; font-size: 13px; display: block;}
	input.error, input.error:focus, input.check.error{border: 1px solid #dd4b39 !important;} 
	select.error, select.error:focus, select.check.error{border: 1px solid #dd4b39 !important;}
	a{color: #77BC1F; font-weight: bold;}
	a:hover{color: #77BC1F; font-weight: bold;}
	.pasos .form-control{
		background: none !important;
		border: 1px solid #C1C1C1;
	}
	
</style>


<?php
//include('menu_customer.php');
$errors = $this->errors;
$order  = $this->order; 

if( !empty($order) ){
	
	$orderHtml = $this->orderHtml;
	$addresses = $order->getAddresses();
	
	$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
	if ($actualLanguage == 'EN'){
		$lang = 0;
	}
	else{
		$lang = 1;
	}
	
	$ordr[0]         = "Order";
	$ordr[1]         = "&Oacute;rden";
	
	$edit_cart[0]    = "Edit Cart";
	$edit_cart[1]    = "Editar Carrito";
	
	$bill_address[0] = "Bill Address";
	$bill_address[1] = "Direcci&oacute;n de Facturaci&oacute;n";
	
	$ship_address[0] = "Ship Address";
	$ship_address[1] = "Direcci&oacute;n de Env&iacute;o";
	
	$cust_info[0]    = "Customer Information";
	$cust_info[1]    = "Informaci&oacute;n del cliente";
	
	$street[0]       = "Street";
	$street[1]       = "Calle";
	
	$city[0]         = "City";
	$city[1]         = "Ciudad";
	
	$estado[0]       = "State";
	$estado[1]       = "Estado";
	
	$pais[0]         = "Country";
	$pais[1]         = "Pa&iacute;s";
	
	$zip[0]          = "Zip Code";
	$zip[1]          = "C&oacute;digo Postal";
	
	$continue[0]     = "Confirm Order";
	$continue[1]     = "Confirmar Orden";
	
	$edit_bill[0]    = "Edit Bill Adress";
	$edit_bill[1]    = "editar Direcci&oacute;n de Facturaci&oacute;n";
	
	$edit_ship[0]    = "Edit ship Adress";
	$edit_ship[1]    = "editar Direcci&oacute;n de Env&iacute;o";
	
	$pur_3[0]        = "Purchase 3 - Ship Address";
	$pur_3[1]        = "Paso 3 - Direcci&oacute;n de env&iacute;o";
	
	$method[0]       = "Payment Method";
	$method[1]       = "M&eacute;todo de Pago";
	
	$comments[0]     = "Comments";
	$comments[1]     = "Comentarios";
	
	$status[0]       = "Order status";
	$status[1]       = "Estado de la &oacute;rden";
	
	$add_inf[0]      = "Additional Information";
	$add_inf[1]      = "Informaci&oacute;n adicional";
	
	$name[0]         = "Name";
	$name[1]         = "Nombre";
	
	$last_name[0]    = "Last Name";
	$last_name[1]    = "Apellido";
	
?>


<div class="content">
	<div class="jumbotron compra">
		<div style="padding-left:220px" class="container">
			<h1 class="h1 lemon">TIENDA EN LÍNEA</h1>
		</div>
    </div>
    <div class="container">
		<div class="row" style="font-family: Arial !important; font-size: 17px;">
			<div class="col-xs-10 col-xs-offset-1" style="border: 1px solid #C1C1C1; background-color: #EEEEEE;">
				<div class="col-xs-7">
					<h2 class="h2" style="text-transform:uppercase; font-family: Arial; font-size: 35px;">
						<?php echo $ordr[$lang]; ?>
					</h2>
				</div>
				<div class="col-xs-5" style="padding: 10px;">
					<?php 
					$profile = eCommerce_SDO_User::LoadUserProfile( $order->getProfileId() );
					?>
					<div style="text-align: right;">Hola, <?php echo $profile->getFirstName().' '.$profile->getLastName();?></div>
					<div style="text-align: right;">A continuación se muestra el detalle del <br/> Pedido #: <span style="color: #77BC1F;"><?php echo $order->getOrderId();?></span></div>
				</div>
				<div class="col-xs-12" style="border-radius: 15px; padding: 15px; border: 1px solid black; background-color: #FFF;">
					<div class="col-xs-6">
						<p>
							<strong>Método de pago :</strong>
							<?php 
							if($order->getPaymentMethod() == 'en_linea'){
								echo 'En L&iacute;nea';
							}elseif($order->getPaymentMethod() == 'deposito'){
								echo "Deposito en efectivo";
							}//end if
							?>
							<br/><br/>
							<strong>Medio de pago :</strong> <?php echo $order->getTipoTarjeta();?><br/><br/>
							<strong>Estatus :</strong> 
							<?php 
							if($order->getStatus() == "C"){
								echo "Orden pagada";
							}elseif($order->getStatus() == "N"){
								echo "Orden pendiente de pago";
							}elseif($order->getStatus() == "R"){
								echo "Pago Rechazado";
							}
							?>
							<br/><br/>
						</p>
					</div>
					<div class="col-xs-6">
						<p class="col-xs-offset-2">
							<strong><?php echo $cust_info[$lang] ?></strong><br>
							<strong><?php  echo $name[$lang];?> :</strong><?php echo $profile->getFirstName();?><br>
							<strong><?php  echo $last_name[$lang];?> :</strong><?php echo $profile->getLastName();?><br>
							<strong>E-mail :</strong><?php echo $profile->getEmail();?><br>


						</p>
						<p class="col-xs-offset-2">
							<strong>Direcci&oacute;n :</strong><br>
							<?php 
							$address = $addresses[ eCommerce_SDO_Order::ADDRESS_SHIP_TO ];
							$country = eCommerce_SDO_CountryManager::GetCountryById( $address->getCountry() );
							$state   = eCommerce_SDO_CountryManager::GetStateById( $address->getState(), $country["country_short_name"] );

							?>
							<?php echo $address->getStreet().' '.$address->getStreet3().'<br>'.$address->getStreet2().'<br>'.$address->getCity().', '.$address->getState().' '.$address->getZipCode();?>' <br>México<br><br> 
						</p>
					</div>
					<div class="clearfix"></div>
					<div class="col-xs-12" align="center">
							<?php 
							if($order->getStatus() == "R"){
								$orderPayment =  eCommerce_SDO_Core_Application_OrderPayment::GetByOrderId($order->getOrderId());
								$message      = eCommerce_FrontEnd_FO_Purchase::decodeResponseCode($orderPayment[0]->getResponsecode());
							?>
							<div class="alert alert-danger">
								<strong><?php echo $message;?></strong> 						
							</div>
							<?php
							}elseif($order->getStatus() == "C"){
							?>
							<div class="alert alert-success">
								<strong>Su pago se proceso exitosamente le enviaremos su pedido a la brevedad posible.</strong> 						
							</div>
							<?php								
							}//end if
							?>	
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-xs-12">
					<h1 class="h2" style="text-transform:uppercase; font-family: Arial; font-size: 35px;">Productos</h1>
				</div>
				<div class="clearfix"></div>
				<div style="border: 1px solid black;">
					<?php echo $orderHtml; ?>
				</div>				
				<div class="clearfix"></div>
				<br/>
				<br/>
				<div align="center">
					<a href="<?=ABS_HTTP_URL?>tienda-en-linea/" class="btn" style="background-image: none; background-color: #000; color: #FFF; font-family: Arial; font-size: 15px;">
						ACEPTAR
					</a>
				</div>
				<br/>
				<br/>
			</div>
		</div>
	</div>
</div>



<?php 
}

include_once ( "footerFlujo.php" );
?>