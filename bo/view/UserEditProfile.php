<?php
include_once ( "header.php" );
//include('menu_customer.php');

function dataToInput( $var ){
	echo "".$var;
}

$user = $this->user;

$strButton= $this->strButton;

$strCmd = $this->strCmd;
$errors= $this->errors;
$ArrRoles = array('Customer');

$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
if ($actualLanguage == 'EN')
{
	$lang = 0;
}
else
{
	$lang = 1;
}

$title1[0] = "Account Information";
$title1[1] = "Informaci&oacute;n de Cuenta";

$title2[0] = "Personal Information";
$title2[1] = "Informaci&oacute;n Personal";

$title3[0] = "Address Information";
$title3[1] = "Informaci&oacute;n de Residencia";

$contra[0] = "Password";
$contra[1] = "Contrase&ntilde;a";

$name[0] = "First Name";
$name[1] = "Nombre(s)";

$last_name[0] = "Last Name";
$last_name[1] = "Apellido(s)";

$street[0] = "Street";
$street[1] = "Calle";

$city[0] = "City";
$city[1] = "Ciudad";

$state[0] = "State";
$state[1] = "Estado";

$country[0] = "Country";
$country[1] = "Pa&iacute;s";

$zip[0] = "Zip Code";
$zip[1] = "C&oacute;digo Postal";

$continue[0] = "Confirm Order";
$continue[1] = "Confirmar Orden";

$edit_bill[0] = "Edit Bill Adress";
$edit_bill[1] = "editar Direcci&oacute;n de Facturaci&oacute;n";

$edit_ship[0] = "Edit ship Adress";
$edit_ship[1] = "editar Direcci&oacute;n de Env&iacute;o";

$pur_3[0] = "Purchase 3 - Ship Address";
$pur_3[1] = "Paso 3 - Direcci&oacute;n de env&iacute;o";

$method[0] = "Payment Method";
$method[1] = "M&eacute;todo de Pago";

$comments[0] = "Comments";
$comments[1] = "Comentarios";


?>
<link href="<?=ABS_HTTP_URL?>css/form.css" rel="stylesheet" type="text/css" />

<br />
<div id="main">
<div class="container">

<div class="row" >
<div class="blanco14"><h1><? echo $this->strSubtitles; ?></h1></div>
<form name="frmUser" id="frmUser" method="post" action="user.php" onsubmit="">

  <div class="error">
		<?php echo ''; ?>
  </div>
	
	<fieldset>
		<legend class=""><?php echo $title1[$lang]; ?></legend>
		
		<dl class="form">
			<dt>E-mail: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[email]" id="email"
				       value="<?php echo dataToInput( $user->getEmail()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("email"); ?>
			
			<dt><?php echo $contra[$lang]; ?>: *</dt>
			<dd>
				<input type="password" size="30" maxlength="100" class="frmInput"
				       name="entity[password]" id="password"
				       value="<?php echo dataToInput( $user->getPassword() ) ?>">
			</dd>
			<?php $errors->getHtmlError("password"); ?>
			
		</dl>
	</fieldset>
	
	<fieldset>
		<legend><?php echo $title2[$lang]; ?></legend>
		
		<dl class="form">
			<dt><?php echo $name[$lang]; ?>: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[first_name]" id="first_name"
				       value="<?php echo dataToInput( $user->getFirstName()  ) ?>">
			</dd>
			<?php $errors->getHtmlError("first_name"); ?>

			<dt><?php echo $last_name[$lang]; ?>: *</dt>
			<dd>
				<input type="text" size="30" maxlength="100" class="frmInput"
				       name="entity[last_name]" id="last_name"
				       value="<?php dataToInput( $user->getLastName()  ) ?>">
			</dd>
			<?php $errors->getHtmlError("last_name"); ?>
			
		</dl>
		
	</fieldset>	   
	<input type="hidden" name="cmd" value="<?php dataToInput( $strCmd  ) ?>">
	
	<input type="submit" value="<?php dataToInput( $strButton  ) ?>" class="btn btn-primary" style="line-height:20px; font-size:12px">
    <input  type="button" value="Cancelar" class="btn btn-primary" style="line-height:20px; font-size:12px" onclick="window.location.href='user_info.php'">
</form>
</div>

</div>

</div>
<?php
include_once ( "footer.php" );
?>