<?php
$header_title='Accesa a tu Cuenta. ¿No Tienes Cuenta? Registrate.';
require_once( "bo/common.php" );

include_once( "headerFlujo.php" );
//include('menu_customer.php');
if(isset($_GET['username'])){
	$username = $_GET['username'];
}
?>
<link rel="stylesheet" type="text/css" href="<?=ABS_HTTP_URL?>css/_styles.css" media="screen" />
<style>
@font-face {
    font-family: 'lemonyellowsunuploaded_file';
    src: url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.eot');
    src: url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.eot?#iefix') format('embedded-opentype'),
         url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.woff2') format('woff2'),
         url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.woff') format('woff'),
         url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.ttf') format('truetype'),
         url('http://beloesoultonic.com/fonts/LemonYellowSun-fonts/2e40d1_0_0-webfont.svg#lemonyellowsunuploaded_file') format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'mf_really_awesomeregular';
    src: url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.eot');
    src: url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.eot?#iefix') format('embedded-opentype'),
         url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.woff2') format('woff2'),
         url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.woff') format('woff'),
         url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.ttf') format('truetype'),
         url('http://beloesoultonic.com/fonts/MFReallyAwsome/mf_really_awesome-webfont.svg#mf_really_awesomeregular') format('svg');
    font-weight: normal;
    font-style: normal;

}
</style>
<style type="text/css">
	.error{color: #E03030; font-family: Arial; font-size: 17px; display: block;}
	input.error, input.error:focus, input.check.error{border: 2px solid #E03030;}	
	select.error, select.error:focus, select.check.error{border: 2px solid #E03030;}
</style>


<div class="home">
	<div class="jumbotron compra">
		<div style="padding-left:220px" class="container">
			<h1 class="h1 lemon">TIENDA EN LÍNEA</h1>
		</div>
    </div>
	<section id="teaser3" class="">
		<div class="container">
			<div class="row">
				<div class="col-xs-12" align="center">
					<div>
						<h1 class="h1" style="font-size: 40px;">Ingresa tu mail para continuar la compra</h1>
					</div>
					<form id="formUserLogin" name="formUserLogin" action="" method="post">
			            <?php 
			            if($this->message != 'Acceso Restringido'){
			                echo  $this->message;
			            }
			            ?>
						<div align="center">
							<input type="email" name="username" id="username" class="h2" value="<?php echo $username;?>" placeholder="su@correo.com" style="font-family: Arial; color: #777777; width: 750px; height: 55px; text-align: center;">
						</div>
						<input type='hidden' name='cmd' value='login' />
						<div class="clearfix"></div>
						<div style="margin-top: 15px;">
							<button type="submit" class="btn" style="font-family: Arial; font-size: 17px; background-color: #000; width: 200px; height: 45px; color: #FFF;">Continuar</button>
						</div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="row" align="center" style="margin-top: 20px;">
				<div style="width: 550px; color: #77BC1F" class="h4">
					Guardamos tu correo electr&oacute;nico de manera 100% segura para :
					<br>
				</div>
				<div style="width: 550px;" align="left">
					<ul>
						<li>Identificar su perfil</li>
						<li>Notificar sobre los estados de su compra</li>
						<li>Guardar el historial de compra</li>
						<li>Facilitar el proceso de compras</li>
					</ul>					
				</div>
			</div>
		</div>
  </section>
  <!--/teaser3 -->
 </div>



<?php
include_once( "footerFlujo.php" );
?>
<script type="text/javascript" src="<?=ABS_HTTP_URL?>js/validate/jquery.validate.js"></script>

<script type="text/javascript">
	$(document).ready(function(e){
		$("#formUserLogin").validate({
			debug: false,
			rules:{
				username:{required: true, email: true},
			},
			messages:{
				username:{required: 'Este campo es obligatorio.', email:'Introduzca un correo electrónico válido, por favor.'},
			},
			submitHandler: function(form){
				document.getElementById("formUserLogin").submit();
			},
		});
	});
</script>