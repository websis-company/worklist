<?php
include_once ( "header.php" );
include('menu_customer.php');

function dataToInput( $value ){
	return htmlentities( $value, ENT_QUOTES, 'UTF-8' );
}

$errors = $this->errors;
$address = $this->address;
//$address = new eCommerce_Entity_User_Address();

$actualLanguage = eCommerce_SDO_LanguageManager::GetActualLanguage();
	if ($actualLanguage == 'EN')
	{
		$lang = 0;
	}
	else
	{
		$lang = 1;
	}
	
	$edt_add[0] = "Edit Address";
	$edt_add[1] = "Editar Direcci&oacute;n";
	
	$title1[0] = "EAddress Information";
	$title1[1] = "Informaci&oacute;n de Direcci&oacute;n";
	
	$street[0] = "Street";
	$street[1] = "Calle";
	
	$street1[0] = "Colonia";
	$street1[1] = "Colonia";
	
	$city[0] = "City";
	$city[1] = "Ciudad";
	
	$estado[0] = "State";
	$estado[1] = "Estado";
	
	$pais[0] = "Country";
	$pais[1] = "Pa&iacute;s";
	
	$zip[0] = "Zip Code";
	$zip[1] = "C&oacute;digo Postal";
	
	$save[0] = "Save";
	$save[1] = "Guardar";
?>
<link href="<?=ABS_HTTP_URL?>css/form.css" rel="stylesheet" type="text/css" />

<div id="main">
<div class="container">

<div class="row">

<div class="blanco14"><h1><?php echo $edt_add[$lang]; ?></h1></div>
<form name="frmUser" id="frmUser" method="post" action="user.php" onsubmit="">

  <div class="error">
		<?php echo $errors->getDescription(); ?>
  </div>
	
	<fieldset>
		<legend><?php echo $title1[$lang]; ?></legend>
		
		<dl class="form">
			<dt><?php echo $street[$lang]; ?>:*</dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="frmInput"
				       name="entity[street]" id="street"
				       value="<?php echo dataToInput( $address->getStreet()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street"); ?>
            
             <dt><?php echo "Número" ?> : </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="frmInput"
				       name="entity[street3]" id="street3"
				       value="<?php echo dataToInput( $address->getStreet3()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street3"); ?>
			
			<dt><?php echo $street1[$lang]; ?>: </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="frmInput"
				       name="entity[street2]" id="street2"
				       value="<?php echo dataToInput( $address->getStreet2()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street2"); ?>
			
			<!-- <dt><?php echo $street[$lang]; ?> 3: </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="frmInput"
				       name="entity[street3]" id="street3"
				       value="<?php echo dataToInput( $address->getStreet3()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street3"); ?>
			
			<dt><?php echo $street[$lang]; ?> 4: </dt>
			<dd>
				<input type="text" size="50" maxlength="100" class="frmInput"
				       name="entity[street4]" id="street4"
				       value="<?php echo dataToInput( $address->getStreet4()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("street4"); ?> -->
			
			<dt><?php echo $zip[$lang]; ?>: *</dt>
			<dd>
				<input type="text" size="5" maxlength="5" class="frmInput"
				       name="entity[zip_code]" id="zip_code"
				       value="<?php echo dataToInput( $address->getZipCode()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("zip_code"); ?>
			
			<dt><?php echo $city[$lang]; ?>: *</dt>
			<dd>
				<input type="text" size="30" maxlength="30" class="frmInput"
				       name="entity[city]" id="city"
				       value="<?php echo dataToInput( $address->getCity()  ) ?>" >
			</dd>
			<?php $errors->getHtmlError("city"); ?>
			
			<dt><?php echo $pais[$lang]; ?>: *</dt>
			<dd>
				<?php
				echo eCommerce_SDO_Core_Application_CountryManager::GetHTMLSelect( 'entity[country]', $address->getCountry(), 'country', 'frmInput' , 'onChange="_refreshSTATE( this.value )"');
				//echo Util_CountryISOCodes::getHTMLSelect( 'entity[country]', $address->getCountry(), 'country', 'frmInput' );
				?>
			</dd>
			<?php $errors->getHtmlError("country"); ?>
			
			<dt><?php echo $estado[$lang]; ?>: *</dt>
			<dd>
				<?php
				$states = eCommerce_SDO_CountryManager::GetStatesOfCountry( $address->getCountry(), true, 'entity[state]', $address->getState(),'state','frmInput', '', true,'' );
			/*
				echo ( !is_array($states)) ? $states : $states["div"];
				
					$states = eCommerce_SDO_CountryManager::GetStatesOfCountry( $address->getCountry() );
					if( !empty($states) ){
						echo "<select name='entity[state]' id='state' class='frmInput'>";
						foreach( $states as $state){
							echo "<option value='". $state['state_short_name'] ."'";
							if($address->getState() == $state['state_short_name']){
								echo " selected='selected'";
							}
							echo ">". $state['name'] ."</option>";
						}
						echo "</select>";
					}
					else{
						echo '<input type="text" size="30" maxlength="30" class="frmInput"
				       name="entity[state]" id="state"
				       value="' . dataToInput( $address->getState()  ).'" >';						
					}
*/
				echo ( !is_array($states)) ? $states : $states["div"];
				?>
			</dd>
			<?php $errors->getHtmlError("state"); ?>
			
		</dl>
		
	</fieldset>
	<input type="hidden" name="entity[profile_id]" value="<?php echo $address->getProfileId() ?>">
	<input type="hidden" name="cmd" value="saveAddress" >
	
	<input type="submit" value="<?=$save[$lang]?>" class="btn btn-primary" style="line-height:20px; font-size:12px">
    <input  type="button" value="Cancelar" class="btn btn-primary" style="line-height:20px; font-size:12px" onclick="window.location.href='user_info.php'">
</form>
</div>

</div>

</div>
<?php
//<a href='purchase.php?nextStep=3'>Next Step</a>
echo ( !is_array($states)) ? '' : $states["formAndAjaxScript"];
?>
<?php
include_once ( "footer.php" );
?>